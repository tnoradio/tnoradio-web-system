import Layout from "../components/layout";
import React from "react";
import { fetchTeam } from "../redux/actions/users/fetchTeam";
import { useStore, connect } from "react-redux";
import UserCard from "../components/userCard";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import { Link } from "@material-ui/core";

const useRowStyles = makeStyles({
  root: {
    background:
      "linear-gradient(to left, rgba(11, 135, 147, 1), rgba(48, 65, 140, 1))",
  },
  cardWrapper: {},
});

var teamList = [];
const Team = (props) => {
  var store = useStore();
  teamList = store.getState().userApp.team;
  const classes = useRowStyles();
  console.log("talentohumano ", props);

  if (teamList !== undefined)
    return (
      <>
        <Layout>
          <Grid className={classes.root} container>
            {teamList.map((teamPlayer) => {
              return (
                <Grid
                  key={teamPlayer._id}
                  className={classes.cardWrapper}
                  item
                  xs={12}
                  sm={6}
                  md={3}
                >
                  <UserCard isTeamPlayer={true} user={teamPlayer} />
                </Grid>
              );
            })}
          </Grid>
        </Layout>
      </>
    );
  else
    return (
      <div>
        <Layout>
          <Grid
            className={classes.progress}
            container
            alignItems="center"
            justifyContent="center"
          >
            <h1>{"Ha ocurrido un problema."}</h1>
          </Grid>
          <Grid
            className={classes.progress}
            container
            alignItems="center"
            justifyContent="center"
          >
            <Link href={"/"} underline="none">
              <h4>{"volver a inicio"}</h4>
            </Link>
          </Grid>
        </Layout>
      </div>
    );
};

const mapStateToProps = (state, ownProps) => {
  return {
    // Mapping users object and Visiblityfilter state to Object
    team: state.userApp.team,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    getTeam: dispatch(fetchTeam()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Team);
