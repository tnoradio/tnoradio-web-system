import React from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import Paper from "@material-ui/core/Paper";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Copyright from "../components/shared/Copyright.js";
import useStylesFunction from "../components/shared/MatUIStyles/useStyles.js";
import Logo from "../components/shared/Logo.js";
import formStylesFunction from "../components/shared/MatUIStyles/formStyles.js";
import UserForm from "../components/forms/UserForm";

const formStyles = formStylesFunction;
const useStyles = useStylesFunction;

export default function CreateUser() {
  const classes = useStyles();
  const formClasses = formStyles();

  return (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={false} sm={4} md={7} className={classes.image} />
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
        <div className={classes.paper}>
          <Box display="flex" justifyContent="center">
            <Logo className={formClasses.logo} form="true" />
          </Box>
          <Typography component="h1" variant="h5">
            Registro de Usuario
          </Typography>
          <UserForm />
          <Box mt={5}>
            <Copyright />
          </Box>
        </div>
      </Grid>
    </Grid>
  );
}
