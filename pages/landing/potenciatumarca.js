import React from 'react';
import { useTheme } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Container from '../../components/landing/common/Container';
import Highlights from '../../components/landing/components/Highlights';
import DemoPages from '../../components/landing/components/DemoPages';
import Hero from '../../components/landing/components/Hero';
import Footer from '../../components/landing/components/Footer';
import Features from  '../../components/landing/components/Features';
import WithLayout from '../../src/WithLayout';
import MainLayout from '../../components/landing/layouts/Main/Main'

const Home = () => {
    const theme = useTheme();
    return (
        <Box>
            <Box bgcolor={theme.palette.alternate.main} position={'relative'}>
                <Container position="relative" zIndex={2}>
                <Hero />
                </Container>
            </Box>
            <Container>
                <Highlights />
            </Container>
            <Container>
                <Features />
            </Container>
            <Box bgcolor={theme.palette.alternate.main}>
                <Container>
                <DemoPages />
                </Container>
            </Box>
            <Container>
                <Footer />
            </Container>
        </Box>
    )}

const Landing = () => {
  return (
    <WithLayout component={Home} layout={MainLayout}/>
  );
};

export default Landing;
