import { Typography, makeStyles, Grid, Link, Divider } from "@material-ui/core";
  import { useEffect, useState } from "react";
import Layout from "../../components/layout";
import parse from 'html-react-parser';
import { useTrackEvent } from "../../hooks/useTrackEvent";
import { fetchPostBySlug } from "../../redux/actions/posts/fetchPostBySlug";
import { fetchPostAuthor } from "../../redux/actions/posts/fetchPostAuthor";
import { convertFromRaw } from "draft-js";
import { stateToHTML } from 'draft-js-export-html';
import { useDispatch, useSelector } from "react-redux";
import RRPlayer from '../../components/player';
import { getPostsPage } from "../../redux/actions/posts/fetchPostsPage";
import Image from 'next/image';
import { relativeTimeRounding } from "moment";


const Post = ({slug}) => {
  const dispatch = useDispatch();
  const state = useSelector((state) => state);
  const useRowStyles = makeStyles((theme) => ({
    title: {
      fontFamily: "RobotoCondensed-Bold",
      color: "rgba(48, 65, 140, 1)",
      paddingTop: "2rem",
      paddingLeft: "3rem",
      paddingBottom: "0.5rem",
      paddingTop: "0px",
      [theme.breakpoints.down("sm")]: {
        fontSize: "2rem",
        paddingLeft: "5%",
        paddingRight: "5%",
      },
    },
    moreContentTitle: {
      fontFamily: "RobotoCondensed-Bold",
      fontSize: "1.5rem",
      color: "rgba(48, 65, 140, 1)",
      paddingTop: "2rem",
      paddingLeft: "3rem",
      paddingBottom: "2.5rem",
      paddingTop: "0px",
      [theme.breakpoints.down("sm")]: {
        padding: "0px",
        paddingLeft: "1rem",
        paddingRight: "1rem",
      },
    },
    otherPostsItemClean: {
      background: "white",
      [theme.breakpoints.down("md")]: {
        height: "0px"
      },
    },
    author: {
      paddingLeft: "3rem",
      paddingBottom: "0.5rem",
      color: "gray",
      fontSize: "1.5rem",

      [theme.breakpoints.down("sm")]: {
        paddingLeft: "5%",
        paddingRight: "5%",
      },
    },
    tags:{
      paddingLeft: "3rem",
      paddingTop: "2rem",
      '& span': {
        fontSize: "1.5rem",
      },
      [theme.breakpoints.down("sm")]: {
        paddingLeft: "5%",
        paddingRight: "5%",
      },
    },
    playerContainer: {
      position: "sticky",
      padding: "1rem",
      height: "100%",

      [theme.breakpoints.down("md")]: {
        position: "relative",
        padding: "0rem",
      },
    },
    otherPostsTitle: {
      fontSize: "1.5rem",
      color: "primary",
      paddingTop: "1rem",
      paddingBottom: "0.5rem",
    },
    otherPostsItem: {
      paddingTop: "1rem",
      background: "white",
    },
    otherPostsContainer: {
      paddingLeft: "1rem",
      paddingTop: "1rem",
      display: "flex",
      [theme.breakpoints.down("sm")]: {
        padding: "0px",
      },
    },
    content: {
      fontFamily: "RobotoCondensed-Light",
      textAlign: "left",
      paddingLeft: "3rem",
      paddingTop: "1%",
      paddingBottom: "2%",

      '& p': {
        fontSize: "1.5rem"
      },

      [theme.breakpoints.down("sm")]: {
        paddingLeft: "5%",
        paddingRight: "5%",
      },

    },
    label: { fontFamily: "RobotoCondensed-Light", fontSize: "0.8rem" },
    image: { paddingTop: "1rem" },
  }));
  const classes = useRowStyles();
  const { trackEvent } = useTrackEvent();
  const [otherPosts, setOtherPosts] = useState([]);
 
  useEffect(() => {
    dispatch(fetchPostBySlug(slug));
  }, [slug]);

    const { title, tags, text, owner_id } = state.postApp?.postBySlug || "";

    useEffect(() => {
      dispatch(fetchPostAuthor(owner_id));
    }, [owner_id]);

    const author = state.postApp.postAuthors.get(owner_id) || "";
    const random = Math.floor(Math.random() * 50) + 1;

    useEffect(async () => {
      const posts = await getPostsPage(5, random);
      setOtherPosts(posts);
    }, [slug]);

    const imageLoader = ({ src }) => {
      return `/${src}`
    }

  return (
    <Layout>
      <Grid container spacing={0}>
        <Grid item xs={12} md={8}>
          <p className={classes.tags}>
            {tags?.map((tag) => (
              <span
                className={
                  (tag === "FRANQUICIAS" ? "info" : "") +
                  (tag === "CULTURA" ? "danger" : "") +
                  (tag === "SALUD" ? "success" : "") +
                  (tag === "EMPRENDIMIENTO" ? "success" : "") +
                  (tag === "ENTRETENIMIENTO" ? "warning" : "") +
                  (tag === "TECNOLOGÍA" ? "success" : "") +
                  (tag === "TECNOLOGIA" ? "success" : "") +
                  (tag === "DEPORTE" ? "info" : "") +
                  (tag === "CINE" ? "purple" : "") +
                  (tag === "EVENTOS" ? "pink" : "") +
                  (tag === "EDUCACIÓN" ? "pink" : "") +
                  (tag === "EDUCACION" ? "purple" : "") +
                  " mr-1"
                }
              >
                {tag}
              </span>
            ))}
          </p>
          <Typography
            className={classes.title}
            variant="h2"
            align="left"
            gutterBottom
          >
            {title}
          </Typography>
          <Typography className={classes.author}>
           {author?.name}
          </Typography>
          <Typography variant="subtitle1" className={classes.content} gutterBottom>
            {text?.blocks?.map((block) => {
              block.text = block.text.replace(/(?:\r\n|\r|\n)/g, '</br>');
              return parse(stateToHTML(convertFromRaw({ blocks: [block], entityMap: {} })));
            }, [])
          }
          </Typography>
        </Grid>
        <Grid item container justifyContent="center" xs={12} md={4} className={classes.playerContainer}>
            <RRPlayer isContent />
        </Grid>
        <Grid item xs={12} md={8} className={classes.otherPostsItem}>
          <Typography className={classes.moreContentTitle}>
            También te puede interesar
            {otherPosts?.map((post) =>
              <Link
                underline="none"
                target="_blank"
                rel="noopener"
                href={`/contenido/${post.slug}`}
                onClick={() => {
                  trackEvent("post_title_click", "posts", "post_title_click", post.title);
                }}
            >
                <Grid container>
                  <Grid item container xs={2} sm={1} justifyContent="center" className={classes.image}>
                    <Image
                          loader={imageLoader}
                          src="ISOTNO5050.webp"
                          layout="fixed"
                          objectFit="contain"
                          width={30}
                          height={30}
                        />
                      </Grid>
                  <Grid item xs={10} sm={11}>
                    <Typography className={classes.otherPostsTitle}>
                        {post.title}
                    </Typography>
                    <Divider variant="middle"/>
                  </Grid>
                </Grid>
               
            </Link>
          )}
        </Typography>
      </Grid>
      <Grid item xs={12} md={4} className={classes.otherPostsItemClean}>
      </Grid>
      </Grid>     
    </Layout>
  );
};

Post.getInitialProps = async ({ query }) => {
  const { slug } = query;
  return { slug };
};


export default Post;
