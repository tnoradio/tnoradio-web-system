import React, { useEffect } from "react";
import Home from "./home";
import Head from "next/head";
import "bootstrap/dist/css/bootstrap.min.css";

const Index = () => {
  return (
    <>
      <Head>
        <title>TNO Radio | La Primera Radio Visual de Venezuela</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <meta
          name="description"
          content="Emisora Radio visual fundada en 2014, transmitimos desde nuestros 3 estudios las 24 horas los 7 días de la semana."
        ></meta>
      </Head>
      <Home></Home>
    </>
  );
};

export default Index;
