import { useRouter } from 'next/router'
import  UserMiniSite  from '../../components/miniSites/users/userMiniSiteComponent'
import { useDispatch } from 'react-redux'
import { fetchUserBySlug } from '../../redux/actions/users/fetchUserBySlug'

const UserSite =  ({userSlug}) => {
    const dispatch = useDispatch()
    if (userSlug !== undefined)
     dispatch(fetchUserBySlug(userSlug.toLowerCase()))
    else
      dispatch(fetchUserBySlug(""))

    return(
    <>
      <UserMiniSite userBySlug={userSlug}></UserMiniSite>
    </>)
}

UserSite.getInitialProps = async ({ query }) => {
  const { userSlug } = query;
  return { userSlug };
};

export default UserSite;
