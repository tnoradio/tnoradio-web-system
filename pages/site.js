import Layout from "../components/layout";
import Grid from "@material-ui/core/Grid";
import Banners from "../components/banners";
import { makeStyles } from "@material-ui/core/styles";
import SideGallery from "../components/sideGallery";
import Social from "../components/socialmedia";
import AudioComponent from "../components/audio/AudioComponent";
import LiveShowContainer from "../containers/liveShowContainer";
import { connect } from "react-redux";
import { fetchShows } from "../redux/actions/shows/fetchShows";
import { showOnline } from "../redux/actions/shows/fetchOnlineShowByTime";
import { getToday } from "../redux/actions/utils/getToday";
import React, { useEffect } from "react";
import { useStore } from "../redux/storeConfig/store";
import { bindActionCreators } from "redux";
//import ConnectedStats from '../components/connectedStats'
import RRPlayer from '../components/player';
import dynamic from 'next/dynamic';

const VPlayer = dynamic(() => import('../components/player'), {
  ssr: false,
});

const useStyles = makeStyles((theme) => ({
  container: {
    backgroundColor: "rgba(48, 65, 140, 1)",
    width: "100%",
    lineHeight: "0 !important",
    height:"100%",
    [theme.breakpoints.down("md")]: {
      minHeight: "492px",
    },
  },
}));

const Site = (props) => {
  const classes = useStyles();
  useEffect(() => {
    const showsList = props.showsList();
  }, [props]);

  useEffect(() => {
    const showOnline = props.showOnline();
  }, [props]);

  return (
    <Layout>
      <Grid container className={classes.container}>
        <Grid item xs={12} md={8}>
          <RRPlayer />
        </Grid>
        <Grid item xs={12} md={4}>
          {/*<AudioComponent></AudioComponent>*/}
          <SideGallery></SideGallery>
          <LiveShowContainer></LiveShowContainer>
        </Grid>
      </Grid>
      <Grid container>
        <Grid item xs={12}>
          <Banners></Banners>
        </Grid>
      </Grid>
      <Grid container>
        <Grid item xs={12}>
          <Social></Social>
        </Grid>
      </Grid>
    </Layout>
  );
};

export async function getStaticProps() {
  // Get external data from the file system, API, DB, etc.

  const showsList = useStore().dispatch(fetchShows());

  useStore().dispatch(showOnline());

  return {
    props: {
      showsList: [],
      showOnline: null,
    },
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    showsList: bindActionCreators(fetchShows, dispatch),
    showOnline: bindActionCreators(showOnline, dispatch),
    today: bindActionCreators(getToday, dispatch),
  };
};

export default connect(null, mapDispatchToProps)(Site);
