import Layout from "../components/layout";
import React from "react";
import { fetchHosts } from "../redux/actions/users/fetchHosts";
import { useStore, connect } from "react-redux";
import UserCard from "../components/userCard";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import { Link } from "@material-ui/core";

const useRowStyles = makeStyles({
  root: {
    background:
      "linear-gradient(to left, rgba(11, 135, 147, 1), rgba(48, 65, 140, 1))",
  },
  cardWrapper: {},
});

var hostsList = [];
const Hosts = (props) => {
  var store = useStore();
  hostsList = store.getState().userApp.hosts;
  const classes = useRowStyles();

  if (hostsList !== undefined)
    return (
      <>
        <Layout>
          <Grid className={classes.root} container>
            {hostsList.map((host) => {
              return (
                <Grid
                  key={host._id}
                  className={classes.cardWrapper}
                  item
                  xs={12}
                  sm={6}
                  md={3}
                >
                  <UserCard user={host} />
                </Grid>
              );
            })}
          </Grid>
        </Layout>
      </>
    );
  else
    return (
      <div>
        <Layout>
          <Grid
            className={classes.progress}
            container
            alignItems="center"
            justifyContent="center"
          >
            <h1>{"Ha ocurrido un problema."}</h1>
          </Grid>
          <Grid
            className={classes.progress}
            container
            alignItems="center"
            justifyContent="center"
          >
            <Link href={"/" + props.user.slug} underline="none">
              <h4>{"volver a inicio"}</h4>
            </Link>
          </Grid>
        </Layout>
      </div>
    );
};

const mapStateToProps = (state, ownProps) => {
  return {
    // Mapping users object and Visiblityfilter state to Object
    hosts: state.userApp.hosts,
  };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    hosts: dispatch(fetchHosts()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Hosts);
