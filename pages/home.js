import Layout from "../components/layout";
import Grid from "@material-ui/core/Grid";
import Banners from "../components/banners";
import { makeStyles } from "@material-ui/core/styles";
import SideGallery from "../components/sideGallery";
import Social from "../components/socialmedia";
import AudioComponent from "../components/audio/AudioComponent";
import LiveShowContainer from "../containers/liveShowContainer";
import { connect } from "react-redux";
import { showOnline } from "../redux/actions/shows/fetchOnlineShowByTime";
import { getToday } from "../redux/actions/utils/getToday";
import React, { useEffect, useState } from "react";
import { useStore } from "../redux/storeConfig/store";
import { bindActionCreators } from "redux";
import ChatBot from "../components/chat/chat";
import FloatingChatBubble from "../components/chat/floatingButton";
import RRPlayer from '../components/player';
 
const useStyles = makeStyles((theme) => ({
  container: {
    backgroundColor: "rgba(48, 65, 140, 1)",
    width: "100%",
    lineHeight: "0 !important",
    [theme.breakpoints.down("md")]: {
      //minHeight: "492px",
    },
  },
}));

const Home = (props) => {
  const classes = useStyles();
  const [showChat, setShowChat] = useState(false);

  console.log(props)

  useEffect(() => {
    const showOnline = props.showOnline();
  }, [props]);

  const toggleShowChat = () => {
    setShowChat((showChat) => !showChat);
  };

  return (
    <div>
      {showChat && (
        <div
          style={{
            position: "fixed",
            right: "20px",
            zIndex: "1000",
            bottom: "100px",
          }}
        >
          <ChatBot></ChatBot>
        </div>
      )}
      <div
        style={{
          position: "fixed",
          right: "20px",
          zIndex: "1000",
          bottom: "10px",
        }}
      >
        <FloatingChatBubble onClick={toggleShowChat} />
      </div>
      <Layout>
        <Grid container className={classes.container}>
          <Grid item xs={12} md={8}>
            <RRPlayer/>
          </Grid>
          <Grid item xs={12} md={4}>
            {/*<AudioComponent></AudioComponent>*/}
            <SideGallery></SideGallery>
            <LiveShowContainer></LiveShowContainer>
          </Grid>
        </Grid>
        <Grid container>
          <Grid item xs={12}>
            <Banners className={classes.banner}></Banners>
          </Grid>
        </Grid>
        <Grid container>
          <Grid item xs={12}>
            <Social></Social>
          </Grid>
        </Grid>
      </Layout>
    </div>
  );
};

export async function getStaticProps() {
  // Get external data from the file system, API, DB, etc.

  useStore().dispatch(showOnline());

  return {
    props: {
      showOnline: null,
    },
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    showOnline: bindActionCreators(showOnline, dispatch),
    today: bindActionCreators(getToday, dispatch),
  };
};

export default connect(null, mapDispatchToProps)(Home);
