import "../styles/globals.scss";
import "fontsource-roboto";
import PropTypes from "prop-types";
import "bootstrap/dist/css/bootstrap.min.css";
import "react-multi-carousel/lib/styles.css";

//For redux usage
import { Provider } from "react-redux";
import { useStore } from "../redux/storeConfig/store";

//Component specific chart CSS
import "../styles/minimalStatisticsCardWithChart.scss";
import "../styles/earningStatisticChartCard.scss";

//Chsrtis CSS
import "chartist/dist/chartist.min.css";

//custom styles
import styles from "../styles/styles.scss";
import "../styles/scss/app.scss";

//Next Components
import Head from "next/head";
import Script from "next/script";

function MyApp({ Component, pageProps }) {
  const store = useStore(pageProps.initialReduxState);

  return (
    <>
      <Head>
        <title>TNO Radio | La Primera Radio Visual de Venezuela</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <meta
          name="description"
          content="Somos la primera radio visual de Venezuela desde enero de 2014."
        ></meta>
        <Script
          id="Adsense-id"
          data-ad-client="ca-pub-2964421302099607"
          async="true"
          strategy="beforeInteractive"
          src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"
          onError={ (e) => { console.error('Adsense script failed to load', e) }}
        />
      </Head>
      <Provider store={store}>
        <Component {...pageProps} />
      </Provider>
    </>
  );
}

export function reportWebVitals(metric) {}

MyApp.propTypes = {
  Component: PropTypes.elementType.isRequired,
  pageProps: PropTypes.object.isRequired,
};

export default MyApp;
