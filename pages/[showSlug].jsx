import  ShowMiniSite  from '../components/miniSites/showMiniSiteComponent'
import { useDispatch } from 'react-redux'
import { fetchShowBySlug } from '../redux/actions/shows/fetchShowBySlug'

const ShowSite =  ({showSlug}) => {

    const dispatch = useDispatch()

    if (showSlug !== undefined)
     dispatch(fetchShowBySlug(showSlug))
    else
      dispatch(fetchShowBySlug(""))

    return(
      <>
        <ShowMiniSite showBySlug={showSlug}></ShowMiniSite>
      </>
    )
}

ShowSite.getInitialProps = async ({ query }) => {
  const { showSlug } = query;
  return { showSlug };
};

export default ShowSite;
