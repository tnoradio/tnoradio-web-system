import ScheduleOfTheDay from "../../components/schedule/scheduleOfTheDay";
import Layout from "../../components/layout";
import ScheduleLayout from "../../components/schedule/scheduleLayout";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { fetchShowsOnDay } from "../../redux/actions/shows/fetchShowsOnDay";
import { bindActionCreators } from "redux";
import { useStore } from "../../redux/storeConfig/store";
import { connect, useDispatch } from "react-redux";
import { Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import CircularProgress from "@material-ui/core/CircularProgress";
import { setSelectedScheduleDay } from "../../redux/actions/utils/setSelectedScheduleDay";

const useStyles = makeStyles((theme) => ({
  root: {
    background: "white",
  },
  container: {
    display: "flex",
  },

  progress: {
    height: "50vh",
  },
}));

var showsOnWeekdayList = [];
const ShowsSchedule = () => {
  showsOnWeekdayList = useStore().getState().showApp.showsOnWeekdayList;
  const router = useRouter();
  const classes = useStyles();
  const { weekDay } = router.query;
  var weekDayParam = weekDay;
  if (weekDay === "miércoles") weekDayParam = "miercoles";
  if (weekDay === "sabado") weekDayParam = "sábado";

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(setSelectedScheduleDay(weekDayParam));
    dispatch(fetchShowsOnDay(weekDayParam));
    showsOnWeekdayList = useStore().getState().showApp.showsOnWeekdayList;
  }, [weekDayParam]);

  return (
    <>
      <Layout>
        <ScheduleLayout>
          {showsOnWeekdayList.length > 0 ? (
            <ScheduleOfTheDay
              showsOnWeekdayList={showsOnWeekdayList}
              weekday={weekDay}
            ></ScheduleOfTheDay>
          ) : null}
        </ScheduleLayout>
      </Layout>
    </>
  );
};

export async function getStaticProps({ params }) {
  // Get external data from the file system, API, DB, etc.
  var showsList = [];

  if (params.weekDay === useStore().getState().utilsApp.today)
    showsList = useStore().getState().showApp.showsOnWeekdayList;
  else {
    useStore().dispatch(fetchShowsOnDay(params.weekDay));
    showsList = useStore().getState().showApp.showsOnWeekdayList;
  }

  return { props: { showsOnWeekdayList: showsList } };
}

export async function getStaticPaths() {
  return {
    paths: [
      { params: { weekDay: "lunes" } },
      { params: { weekDay: "martes" } },
      { params: { weekDay: "miércoles" } },
      { params: { weekDay: "miercoles" } },
      { params: { weekDay: "jueves" } },
      { params: { weekDay: "viernes" } },
      { params: { weekDay: "sábado" } },
      { params: { weekDay: "sabado" } },
      { params: { weekDay: "domingo" } },
    ],
    fallback: false,
  };
}

const mapStateToProps = (state) => {
  return { showsOnWeekdayList: state.showApp.showsOnWeekdayList };
};

const mapDispatchToProps = (dispatch) => {
  return {
    showsOnWeekdayList: bindActionCreators(fetchShowsOnDay, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ShowsSchedule);
