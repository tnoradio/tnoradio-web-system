import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Copyright from '../../components/shared/Copyright.js';
import useStylesFunction from '../../components/shared/MatUIStyles/useStyles.js';
import Logo from '../../components/shared/Logo.js';
import formStylesFunction from '../../components/shared/MatUIStyles/formStyles.js';
import { useRouter } from 'next/router'

const formStyles = formStylesFunction;
const useStyles = useStylesFunction;

export default function Registered() {
  const router = useRouter()
  const classes = useStyles();
  const formClasses = formStyles();
  const { userRegistered } = router.query

  return (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={12} sm={12} md={12} component={Paper} elevation={6} square>
        <div className={classes.paper}>
          <Box display="flex" justifyContent="center" className={formClasses.logo}>
              <Logo />
          </Box>
          <Typography component="h1" variant="h5">
            Usuario {userRegistered} Registrado
          </Typography>
          <Box mt={5}>
            <Copyright />
          </Box>
        </div>
      </Grid>
    </Grid>
  );
}