
export type CreateUserRequest = {
    id: string;
    name: string;
    lastName: string;
    DNI: number;
    email: string;
    isActive: Boolean;
    password: string;
    passordConfirmation: string;
    gender: string;
  };