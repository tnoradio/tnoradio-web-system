import { User } from '../../../domain/User'
import { UserEmail } from '../../../domain/UserEmail';
import { UserRepository } from '../../../domain/UserRepository'
import { CreateUserRequest } from './CreateUserRequest'

export class UserCreator {
    
 private repository: UserRepository;

  constructor(repository: UserRepository) {
    this.repository = repository;
  }

  async run(request: CreateUserRequest): Promise<User> {
    const user = User.create(
      request.id,
      request.name,
      request.lastName,
      request.DNI,
      new UserEmail(request.email),      
      request.isActive,
      request.password,
      request.passordConfirmation,
      request.gender
    );

    return await this.repository.save(user);
  }
}