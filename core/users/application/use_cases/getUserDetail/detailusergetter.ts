import { UserRepository } from '../../../domain/UserRepository'

export class DetailUserGetter {
    
 private repository: UserRepository;

  constructor(repository: UserRepository) {
    this.repository = repository;
  }

  async run(slug: string): Promise<any> {
    return await this.repository.getUserBySlug(slug);
  }
}