import { UserEmail } from './UserEmail'

export class User {
    readonly id: string;
    readonly name: string;
    readonly lastName: string;
    readonly DNI: Number;
    readonly email: UserEmail;
    readonly isActive: Boolean;
    readonly password: string;
    readonly passwordConfirmation: string;
    readonly gender: string;
  
    constructor(
        id:string, name: string, lastName: string, 
        DNI: Number, email: UserEmail, isActive: Boolean, 
        password: string, passwordConfirmation:string, gender:string) {
            this.id = id;
            this.name = name;
            this.lastName = lastName;
            this.DNI = DNI;
            this.email = email;
            this.isActive = isActive;
            this.password = password;
            this.gender = gender;
        }

    static create(
        id:string, name: string, lastName: string, 
        DNI: Number, email: UserEmail, isActive: Boolean,
        password:string, passwordConfirmation:string, gender:string){
        
        return new User(id,name,lastName,DNI,email,isActive, 
                        password,passwordConfirmation,gender);
    }

    static createFromUser (user:any){}


}  