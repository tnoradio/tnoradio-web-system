export class UserImage {
    _id: String;
    imageName: String;
    imageUrl: String;
    file:
        {
            data: Buffer,
            contentType: String
        }

    constructor(
        _id: String, imageName: String, imageUrl: String, file:{data: Buffer, contentType: String}) {
        this._id = _id;
        this.imageName = imageName;
        this.imageUrl = imageUrl;
        this.file = file;
    }

    static create(
        _id: String, imageName: String, imageUrl: String, file:{data: Buffer, contentType: String}) {

        return new UserImage(_id, imageName, imageUrl, file);
    }
}  