import { User } from "./User";
import { UserEmail } from "./UserEmail";

export interface UserRepository {
  save(user: User): Promise<User>;
  isValidEmail(email: string): Promise<boolean>;
  getUserBySlug(slug: string): Promise<User>;
  getUserById(_id: string): Promise<User>;
  getHosts(): Promise<User[]>;
  getTeam(): Promise<User[]>;
  getUserImage(name, slug): Promise<any>;
  /*updateUser(user:User): Promise<User>;
  removeUser(userId:string):Promise<User>;
  
  getByEmail(email:string):Promise<User>;
  index():Promise<User[]>;*/
}
