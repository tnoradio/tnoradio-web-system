import { ShowSchedule }  from './ShowSchedule'
import { ShowColor } from './ShowColor'
import { ShowSocialMedia } from './ShowSocialMedia'
import { ShowImage } from './ShowImage'
import { ShowHost } from './ShowHost';
import { ShowAdvertiser } from './ShowAdvertiser';


export class Show {
    id: Number;
    name: string;
    genre: string;
    synopsis: string;
    type: string;
    showSchedules: ShowSchedule[];
    email: string;
    isEnabled: Boolean;
    pgClassification: string;
    initDate: string;
    producer: string;
    socials: ShowSocialMedia[];
    hosts: ShowHost[];
    advertisers: ShowAdvertiser[];
    images: ShowImage[];
    showSlug: string;
    colors: ShowColor[];
  
    constructor(
        id:Number, name: string, genre: string, synopsis: string, type: string, showSchedule: ShowSchedule[],
        email: string, isEnabled: Boolean, pgClassification: string, initDate: string,
        producer: string, socials: ShowSocialMedia[], hosts: ShowHost[], advertisers: ShowAdvertiser[],
        images: ShowImage[], showSlug: string, colors: ShowColor[]) {
            this.id = id;
            this.name = name;
            this.genre = genre;
            this.synopsis = synopsis;
            this.type = type;
            this.showSchedules = showSchedule;
            this.email = email;
            this.isEnabled = isEnabled;
            this.pgClassification = pgClassification;
            this.initDate = initDate;
            this.producer = producer;
            this.socials = socials;
            this.hosts = hosts;
            this.advertisers = advertisers;
            this.images = images;
            this.showSlug = showSlug;
            this.colors = colors;
    }

    static create(
        id:Number, name: string, genre: string, synopsis: string, type: string, showSchedule: ShowSchedule[],
        email: string, isEnabled: Boolean, pgClassification: string, initDate: string,
        producer: string, socials: ShowSocialMedia[], hosts: ShowHost[], advertisers: ShowAdvertiser[],
        images: ShowImage[], showSlug: string, colors: ShowColor[]){
        
        return new Show(id, name, genre, synopsis, type, showSchedule, email, isEnabled, 
                        pgClassification, initDate, producer, socials, hosts, advertisers, images, showSlug, colors);
    }

    static createFromShow (show: any){

        return new Show(show.id, show.name, show.genre, show.synopsis, 
            show.type, show.showSchedule, show.email, show.isEnabled, show.pgClassification, 
            show.initDate, show.producer, show.socials, show.hosts, show.advertisers, 
            show.images, show.showSlug, show.colors
        );
    }
}  