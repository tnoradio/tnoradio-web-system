export class ShowAdvertiser {
    id: Number;
    showId: Number;    
    name: string;
    imageUrl: string;
    website: string;  
  
    constructor(  
        id: Number,
        showId: Number,    
        name: string,
        imageUrl: string,
        website: string
        ) {
            this.id = id,
            this.showId = showId;
            this.name = name;
            this.imageUrl = imageUrl;
            this.website = website;
        }

    static create(
        id: Number,
        showId: Number,    
        name: string,
        imageUrl: string,
        website: string
        )
    {
        
        return new ShowAdvertiser(id, showId, name, imageUrl, website);
    }

    static createFromShowAdvertiser(showAdvertiser: any){

        return new ShowAdvertiser(showAdvertiser.id, showAdvertiser.showId,  showAdvertiser.name, 
            showAdvertiser.imageUrl, showAdvertiser.website);
    }
}  