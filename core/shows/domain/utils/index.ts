import formatTime from  './formatTime'
import sortSchedule from './sortSchedule'
import getTimeFromDate from './getTimeFromDate'
import shortenDay from './shortenDay'

export default { 
    formatTime: formatTime, 
    sortSchedule: sortSchedule, 
    getTimeFromDate: getTimeFromDate, 
    shortenDay: shortenDay 
}