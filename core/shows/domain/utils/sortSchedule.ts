import { IShowScheduleRow } from '../../../../interfaces/IShowScheduleRow'

/**
 * 
 * @param schedules Sorts shows by start time
 */
const sortSchedule = (dataRows: IShowScheduleRow[]): IShowScheduleRow[] => {
    dataRows.sort((a,b) => {
      let keyA = a.startTime;
      let keyB = b.startTime;
      // Compare the 2 dates
      if (keyA < keyB) return -1;
      if (keyA > keyB) return 1;
      return 0;
    })
  
    return dataRows;
  }
  

  export default sortSchedule;