export class ShowImage {
  id: Number;
  name: string;
  showSlug: string;
  file: {
    data: Buffer;
    contentType: String;
  };
  show: number;
  url: string;

  constructor(
    id: Number,
    name: string,
    showSlug: string,
    file: {
      data: Buffer;
      contentType: String;
    },
    show: number,
    url: string
  ) {
    this.id = id;
    this.name = name;
    this.showSlug = showSlug;
    this.file = file;
    this.show = show;
    this.url = url;
  }

  static create(
    id: Number,
    name: string,
    showSlug: string,
    file: {
      data: Buffer;
      contentType: String;
    },
    show: number,
    url: string
  ) {
    return new ShowImage(id, name, showSlug, file, show, url);
  }

  static createFromShowImageUrls(showImage: any) {
    return new ShowImage(
      showImage.id,
      showImage.name,
      showImage.showSlug,
      showImage.file,
      showImage.show,
      showImage.url
    );
  }
}
