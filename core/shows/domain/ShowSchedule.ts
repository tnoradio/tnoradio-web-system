export class ShowSchedule {
    id: Number;
    startTime: Date;
    endTime: Date;
    weekDay: string;
  
    constructor(
        id:Number, startTime: Date, endTime: Date, weekDay: string) {
            this.id = id;
            this.startTime = startTime;
            this.endTime = endTime;
            this.weekDay = weekDay
        }

    static create(
        id:Number, startTime: Date, endTime: Date, weekDay: string){
        
        return new ShowSchedule(id, startTime, endTime, weekDay);
    }

    static createFromShowSchedule (showSchedule: any){

        return new ShowSchedule(showSchedule.id, showSchedule.startTime, showSchedule.endTime, showSchedule.weekDay);
    }
}  