import { Show } from "./Show";
import { ShowAdvertiser } from "./ShowAdvertiser";
import { ShowImage } from "./ShowImage";

export interface ShowRepository {
  index(): Promise<Show[]>;
  indexEnabled(): Promise<Show[]>;
  getShowsOnWeekday(weekday: string): Promise<Show[] | []>;
  getShow(showLink: string): Promise<Show>;
  getOnlineShowByTime(time: string): Promise<Show>;
  getShowBySlugWithHosts(slug: string): Promise<any>;
  getShowBanners(imageName: String): Promise<ShowImage[]>;
  getShowImage(imageName: String, showSlug: String): Promise<ShowImage>;
  getShowAdvertisers(showId: string): Promise<ShowAdvertiser[]>;
}
