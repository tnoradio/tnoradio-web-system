
export interface OnDemandRepository {
    getShowNames(showSlug: String): Promise<any[]>;
  }
  