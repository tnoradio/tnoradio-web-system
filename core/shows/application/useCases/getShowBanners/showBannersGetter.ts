import { ShowRepository } from '../../../domain/ShowRepository'

export class ShowBannersGetter {
    
 private repository: ShowRepository;

  constructor(repository: ShowRepository) {
    this.repository = repository;
  }

  async run(imageName): Promise<any> {
    return await this.repository.getShowBanners(imageName);
  }
}