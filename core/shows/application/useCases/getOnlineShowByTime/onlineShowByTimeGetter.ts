import { ShowRepository } from '../../../domain/ShowRepository'
import { Show } from '../../../domain/Show'

export class OnlineShowByTimeGetter {
    
 private repository: ShowRepository;

  constructor(repository: ShowRepository) {
    this.repository = repository;
  }

  async run(time: string): Promise<Show> {
    return await this.repository.getOnlineShowByTime(time);
  }
}