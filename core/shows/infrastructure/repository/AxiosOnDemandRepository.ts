import axios from "axios";
import { OnDemandRepository } from "../../domain/OnDemandRepository";

const baseUrl = process.env.NEXT_API_CDN_URL || "";
//const baseUrl = "http://127.0.0.1:5000/";

export class AxiosOnDemandRepository implements OnDemandRepository {
  async getShowNames(showSlug: String): Promise<any[]> {
    console.log(`${baseUrl}get_shows`)
    try {
        const response = await axios.get(`${baseUrl}get_shows`, {
            params: { show_slug: showSlug },
        });
        return response.data;
      } catch (error) {
        console.log("AxiosOnDemandRepository", error);
        return error;
      }
  }
}
