import TextInput from "../../domain/TextInput";
import { ChatbotService } from "../../domain/ChatbotService";

export class TextInputRequest {
  private service: ChatbotService;

  constructor(service: ChatbotService) {
    this.service = service;
  }

  async run(request: TextInput): Promise<any> {
    return await this.service.textInputRequest(request);
  }
}
