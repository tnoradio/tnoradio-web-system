import TextInput from "./TextInput";

export interface ChatbotService {
  textInputRequest(textInput: TextInput): Promise<any>;
}
