export default class Message {
  message: string;
  sentTime: Date;
  sender: string;
  direction: string;
  position: string;

  constructor(
    message: string,
    sentTime: Date,
    sender: string,
    direction: string,
    position: string
  ) {
    this.message = message;
    this.sentTime = sentTime;
    this.sender = sender;
    this.direction = direction;
    this.position = position;
  }
}
