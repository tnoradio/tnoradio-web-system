export default class TextInput {
  message: string;

  constructor(message: string) {
    this.message = message;
  }

  static create(message: string) {
    return new TextInput(message);
  }
}
