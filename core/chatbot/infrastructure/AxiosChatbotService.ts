/**
 * Implements Post repository interface with axios
 * promise based HTTP client.
 */
import axios from "axios";
import { ChatbotService } from "../domain/ChatbotService";
import TextInput from "../domain/TextInput";

const baseURL = process.env.NEXT_API_CHATBOT_URL;
//const baseURL = "http://localhost:12000/api/chatbot/";

export class AxiosChatbotService implements ChatbotService {
  async textInputRequest(textInput: TextInput): Promise<any> {
    console.log("AxiosChatbotService ", textInput.message);
    try {
      const response = await axios.post(baseURL + "textinputrequest", {
        textInput: textInput.message,
      });
      return response.data;
    } catch (error) {
      if (error.response) {
        return Promise.reject(error.response);
      } else {
        let errorObject = JSON.parse(JSON.stringify(error));
        if (errorObject.message) {
          return Promise.reject(errorObject);
        } else {
          return Promise.reject(error);
        }
      }
    }
  }
}
