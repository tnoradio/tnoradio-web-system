import { TextBlock } from "../../../domain/Post";
import PostImage from "../../../domain/PostImage";

export type CreatePostRequest = {
  _id: String;
  title: String;
  owner_id: String; // user id of owner
  subtitle: String;
  summary: String;
  image: String;
  images: PostImage[];
  publish_date: Date;
  tags: String[];
  starred: Boolean;
  deleted: Boolean;
  text:  { blocks: TextBlock[]; entityMap: {}; blockMap: {}; selectionAfter: {}; selecttionBefore: {} };
  slug: String;
  approved: Boolean;
};
