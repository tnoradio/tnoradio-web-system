import { PostRepository } from "../../../domain/PostRepository";

export class DetailUserGetter {
  private repository: PostRepository;

  constructor(repository: PostRepository) {
    this.repository = repository;
  }

  async run(postId: string): Promise<any> {
    return await this.repository.getPostById(postId);
  }
}
