import { Tag } from "reactstrap";
import Post from "../../posts/domain/Post";
import PostImage from "../../posts/domain/PostImage";

export interface PostRepository {
  save(post: Post): Promise<Post | Error>;
  saveTag(tag: Tag): Promise<Tag | Error>;
  getPostById(_id: String): Promise<Post>;
  getPostBySlug(slug: String): Promise<Post>;
  getAll(): Promise<Post[]>;
  getPostsPage(pageSize: number, page: number): Promise<Post[]>;
  getPostsPageByOwner(
    pageSize: number,
    page: number,
    owner: String
  ): Promise<Post[]>;
  getAllTags(): Promise<Tag[] | Error>;
  update(post: Post, id?: String): Promise<Post | Error>;
  destroy(postId: String): Promise<any | Error>;
  delete(postId: String): Promise<Post | Error>;
  getPostImage(name: String, slug: String): Promise<PostImage>;
  savePostImage(image): Promise<PostImage>;
  updatePostImage(image, slug, name): Promise<PostImage>;
}
