import React from "react";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
//import LogoNegativo from '../../LogoNegativo.png'
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import TwitterIcon from "@material-ui/icons/Twitter";
import FacebookIcon from "@material-ui/icons/Facebook";
import InstagramIcon from "@material-ui/icons/Instagram";
import LinkedInIcon from "@material-ui/icons/LinkedIn";
import YouTubeIcon from "@material-ui/icons/YouTube";
import Link from "@material-ui/core/Link";

const useStyles = makeStyles((theme) => ({
  footer: {
    background: "url('/bkgs/bkgd-profile.jpg') no-repeat",
    backgroundImage:
      "linear-gradient(to bottom, rgba(11, 135, 147, 1), rgba(48, 65, 140, 1)), url('/bkgs/bckg-home-footer.jpg') no-repeat",
    backgroundSize: "cover",
    backgroundPositionY: "center",
    color: "white",
    paddingTop: "1.1em",
    paddingBottom: "1.1em",
  },
  box: {
    padding: "2em",
  },
  typo: {
    color: "white",
    minWidth: "36px !important",
    fontFamily: "RobotoCondensed-Bold",
  },
  listItem: {
    padding: "0 !important",
    color: "white",
  },
  listText: {
    color: "white",
  },
  logo: {
    width: "40%",
  },
}));

export default function CommunityList() {
  const classes = useStyles();
  return (
    <div>
      <Typography
        className={classes.typo}
        variant="h4"
        style={{ textTransform: "uppercase" }}
      >
        Síguenos
      </Typography>
      <Divider />
      <List component="nav">
        <ListItem className={classes.listItem} button>
          <ListItemIcon className={classes.typo}>
            <TwitterIcon />
          </ListItemIcon>
          <ListItemText
            disableTypography
            secondary={
              <Typography
                type="caption"
                style={{
                  color: "#FFFFFF",
                  fontSize: "1.2em",
                  fontFamily: "RobotoCondensed-Light",
                }}
              >
                <Link
                  target="_blank"
                  rel="noopener"
                  color="inherit"
                  href="https://www.twitter.com/tnoradio"
                  underline="none"
                >
                  Twitter
                </Link>
              </Typography>
            }
          />
        </ListItem>
        <ListItem className={classes.listItem} button>
          <ListItemIcon className={classes.typo}>
            <InstagramIcon />
          </ListItemIcon>
          <ListItemText
            disableTypography
            secondary={
              <Typography
                type="caption"
                style={{
                  color: "#FFFFFF",
                  fontSize: "1.2em",
                  fontFamily: "RobotoCondensed-Light",
                }}
              >
                <Link
                  target="_blank"
                  rel="noopener"
                  href="https://www.instagram.com/tnoradio"
                  color="inherit"
                  underline="none"
                >
                  Instagram
                </Link>
              </Typography>
            }
          />
        </ListItem>
        <ListItem className={classes.listItem} button>
          <ListItemIcon className={classes.typo}>
            <FacebookIcon />
          </ListItemIcon>
          <ListItemText
            disableTypography
            secondary={
              <Typography
                type="caption"
                style={{
                  color: "#FFFFFF",
                  fontSize: "1.2em",
                  fontFamily: "RobotoCondensed-Light",
                }}
              >
                <Link
                  target="_blank"
                  rel="noopener"
                  href="https://www.facebook.com/tnoradio"
                  color="inherit"
                  underline="none"
                >
                  Facebook
                </Link>
              </Typography>
            }
          />
        </ListItem>
        <ListItem className={classes.listItem} button>
          <ListItemIcon className={classes.typo}>
            <YouTubeIcon />
          </ListItemIcon>
          <ListItemText
            disableTypography
            secondary={
              <Typography
                type="caption"
                style={{
                  color: "#FFFFFF",
                  fontSize: "1.2em",
                  fontFamily: "RobotoCondensed-Light",
                }}
              >
                <Link
                  target="_blank"
                  rel="noopener"
                  href="https://youtube.com/channel/UC9h-Wfxxf4MHNxEZux9q7Qw"
                  color="inherit"
                  underline="none"
                >
                  Youtube
                </Link>
              </Typography>
            }
          />
        </ListItem>
        <ListItem className={classes.listItem} button>
          <ListItemIcon className={classes.typo}>
            <LinkedInIcon />
          </ListItemIcon>
          <ListItemText
            disableTypography
            secondary={
              <Typography
                type="caption"
                style={{
                  color: "#FFFFFF",
                  fontSize: "1.2em",
                  fontFamily: "RobotoCondensed-Light",
                }}
              >
                <Link
                  target="_blank"
                  rel="noopener"
                  href="https://ve.linkedin.com/company/tno-radio"
                  color="inherit"
                  underline="none"
                >
                  LinkedIn
                </Link>
              </Typography>
            }
          />
        </ListItem>
      </List>
    </div>
  );
}
