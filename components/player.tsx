import { useTrackEvent } from "../hooks/useTrackEvent";
import { useCheckNetworkSpeed }  from "../hooks/useCheckNetworkSpeed";
import { makeStyles } from "@material-ui/core/styles";
import '@vime/core/themes/default.css';



const RRPlayer = ({isContent=false}) => {
  const useStyles = makeStyles(() => ({
    playerContainer: {
      position: "relative",
      paddingBottom: "56.25%", /* 16:9 */
      height: 0,
      width: isContent ? "100%" : "unset",
      background: "rgba(48, 65, 140, 1)",
  
      '& iframe': {
        position: "absolute",
        top: 0,
        left: 0,
        width: "100%",
        height: "100%",
      }
    },
  }));
  const classes = useStyles();
  const { trackEvent } = useTrackEvent();
  const networkSpeed = useCheckNetworkSpeed();
  const url = networkSpeed < 5 ? "https://playerv.srvstm.com/video/tnovideo2//true/true/YzNSdGRqRXVjM0oyYzNSdExtTnZiUT09K0Q=/16:9/aHR0cDovL3Rub3JhZGlvLmNvbS9Mb2dvX1ROT18yMDIxX3doaXRlLnBuZysw/nao/nao"  : 
  "https://playerv.srvstm.com/video/tnovideo1//true/true/YzNSdGRqRXVjM0oyYzNSdExtTnZiUT09K0Q=/16:9/aHR0cDovL3Rub3JhZGlvLmNvbS9Mb2dvX1ROT18yMDIxX3doaXRlLnBuZysw/nao/nao";

  return (
    <div 
      onClick={() => trackEvent("player_click", "click", "click", "Player")}
      className={classes.playerContainer}
      >
      <iframe 
        width="1280" 
        height="700" 
        src={url} 
        allowFullScreen>
      </iframe>
    </div>
  )
}

export default RRPlayer;
