import { useRouter } from "next/router";
import { makeStyles } from "@material-ui/core/styles";
import React, { useState } from "react";
import { Navbar, Nav, Collapse, NavLink, NavItem } from "reactstrap";
import { useSelector } from "react-redux";

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      padding: "0px",
    },
    "&:hover": {
      cursor: "pointer",
    },
  },
  link: {
    fontSize: "1.8em",
    fontWeight: "600",
    lineHeight: "1em",
    color: "rgba(48, 65, 140, 1) !important",
    [theme.breakpoints.between("xs", "md")]: {
      fontSize: "1em",
      //backgroundColor: 'red',
    },
    [theme.breakpoints.down("xs")]: {
      fontSize: "0.6em",
      lineHeight: "0.8em",
    },
    ["@media (max-width:475px)"]: {
      fontSize: "0.5em",
      lineHeight: "0.7em",
    },
    "&:hover": {
      cursor: "pointer",
      //color:"white !important",
      //backgroundColor:"rgba(48, 65, 140, 1) !important",
      fontWeight: 900,
    },
    "&.active": {
      color: "white !important",
      backgroundColor: "rgba(48, 65, 140, 1) !important",
    },
  },
  border: {
    borderRight: "solid",
    borderRightColor: "rgba(11, 135, 147, 1) !important",
    borderRightWidth: "thin",
  },
  day: {
    fontFamily: "RobotoCondensed-Bold",
    width: "80px",
    fontSize: "1.9rem",
    textAlign: "center",
    "& a": {
      "&:hover": {
        "& span": {
          color: "rgba(48, 65, 140, 1)",
          //color:"white",
          //paddingLeft:"10px",
          //paddingRight:"10px",
          //textAlign:"center",
          fontWeight: 700,
          [theme.breakpoints.down("md")]: {
            paddingLeft: "10px",
            paddingRight: "10px",
          },
        },
      },
    },

    [theme.breakpoints.down("xs")]: {
      width: "30px",
      fontSize: "0.7rem",
    },
  },
  activeDay: {
    width: "80px",
    fontSize: "1.9rem",
    //textAlign: "center",
    textDecoration: "underline",
    color: "rgba(48, 65, 140, 1) !important",
    //color:"white",
    //paddingLeft:"10px",
    //paddingRight:"10px",
    //textAlign:"center",
    fontFamily: "RobotoCondensed-Bold",
    "& span": {
      color: "rgba(48, 65, 140, 1) !important",
    },
    [theme.breakpoints.down("xs")]: {
      fontSize: "0.7rem",
      width: "30px",
      //backgroundColor: 'red',
    },
  },
  title: {
    fontSize: "1.6em",
    fontWeight: "600",
    lineHeight: "1em",
    color: "rgba(48, 65, 140, 1) !important",
    ["@media (max-width:767px)"]: {
      fontSize: "1em",
      //backgroundColor: 'red',
    },
    ["@media (max-width:475px)"]: {
      fontSize: "0.6em",
      //backgroundColor: 'red',
    },
  },
}));

/**
 * This component will be shared across all pages.
 */

export default function ScheduleNavbar() {
  const classes = useStyles();
  const state = useSelector((state) => state);
  const router = useRouter();
  const [weekDay, setWeekday] = useState("");
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);

  React.useEffect(() => {
  }, [weekDay]);

  return (
    <Navbar color="light" light expand="xs">
      <Collapse isOpen={isOpen} navbar>
        <Nav
          className="navbar-collapse collapse justify-content-center order-2"
          navbar
        >
          <NavItem
            className={weekDay === "lunes" ? classes.activeDay : classes.day}
          >
            <NavLink
              onClick={() => {
                setWeekday(() => "lunes");
                router.push({ pathname: "/programacion/lunes" }, undefined, {
                  shallow: true,
                });
              }}
            >
              <span>LUN</span>
            </NavLink>
          </NavItem>
          <NavItem
            className={weekDay === "martes" ? classes.activeDay : classes.day}
          >
            <NavLink
              onClick={() => {
                setWeekday(() => "martes");
                router.push({ pathname: "/programacion/martes" }, undefined, {
                  shallow: true,
                });
              }}
            >
              <span>MAR</span>
            </NavLink>
          </NavItem>
          <NavItem
            className={
              weekDay === "miercoles" ? classes.activeDay : classes.day
            }
          >
            <NavLink
              onClick={() => {
                setWeekday(() => "miercoles");
                router.push(
                  { pathname: "/programacion/miercoles" },
                  undefined,
                  { shallow: true }
                );
              }}
            >
              <span>MIER</span>
            </NavLink>
          </NavItem>
          <NavItem
            className={weekDay === "jueves" ? classes.activeDay : classes.day}
          >
            <NavLink
              onClick={() => {
                setWeekday(() => "jueves");
                router.push({ pathname: "/programacion/jueves" }, undefined, {
                  shallow: true,
                });
              }}
            >
              <span>JUE</span>
            </NavLink>
          </NavItem>
          <NavItem
            className={weekDay === "viernes" ? classes.activeDay : classes.day}
          >
            <NavLink
              onClick={() => {
                setWeekday(() => "viernes");
                router.push({ pathname: "/programacion/viernes" }, undefined, {
                  shallow: true,
                });
              }}
            >
              <span>VIER</span>
            </NavLink>
          </NavItem>
          {/** check how is saturday saving, with or without accent to make this change */}
          <NavItem
            className={weekDay === "sábado" ? classes.activeDay : classes.day}
          >
            <NavLink
              onClick={() => {
                setWeekday(() => "sábado");
                router.push({ pathname: "/programacion/sabado" }, undefined, {
                  shallow: true,
                });
              }}
            >
              <span>SÁB</span>
            </NavLink>
          </NavItem>
          <NavItem
            className={weekDay === "domingo" ? classes.activeDay : classes.day}
          >
            <NavLink
              onClick={() => {
                setWeekday(() => "domingo");
                router.push({ pathname: "/programacion/domingo" }, undefined, {
                  shallow: true,
                });
              }}
            >
              <span>DOM</span>
            </NavLink>
          </NavItem>
        </Nav>
      </Collapse>
    </Navbar>
  );
}
