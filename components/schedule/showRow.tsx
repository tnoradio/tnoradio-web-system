import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Collapse from "@material-ui/core/Collapse";
import IconButton from "@material-ui/core/IconButton";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Typography from "@material-ui/core/Typography";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@material-ui/icons/KeyboardArrowUp";
import MiniBannerComponent from "./miniBannerComponent";
import Hidden from "@material-ui/core/Hidden";
import utils from "../../core/shows/domain/utils";
import OtherSchedules from "./otherSchedules";
import { useRouter } from "next/router";
import { useDispatch } from "react-redux";
import { useStore } from "../../redux/storeConfig/store";

/**
 * Receives a  list with the row data and renders a table with
 * show rows.
 * @param props
 */
const Row = (props) => {
  const store = useStore();
  const { row } = props;
  const [open, setOpen] = useState(false);
  const [miniBannerUrl, setMiniBannerUrl] = useState("");
  const router = useRouter();
  const dispatch = useDispatch();

  const useRowStyles = makeStyles((theme) => ({
    otherRowHeader: {
      "& > *": {
        padding: "0px",
      },
    },
    root: {
      "& > *": {
        padding: "0px",
        background: open ? "rgba(48, 65, 140, 1) " : "white ",
        color: open ? "white !important" : "rgba(48, 65, 140, 1) !important",
      },
      "& tr": {
        "&:hover": {
          background: "red !important",
        },
      },
    },
    button: {
      paddingLeft: "0.5em",
      paddingRight: "2em",
      ["@media (max-width:475px)"]: {
        paddingRight: ".5em",
      },
    },
    miniBanner: {
      background: "rgba(48, 65, 140, 1)",
      "&:hover": { cursor: "pointer " },
    },
    showTitleCell: {
      fontFamily: "RobotoCondensed-Bold",
      paddingLeft: ".3em",
      textTransform: "uppercase",
      "&:hover": {
        cursor: "pointer",
        background: "rgba(48, 65, 140, 1)",
        color: "white !important",
      },
    },
    showTitle: {
      fontSize: "1.6em",
      verticalAlign: "middle !important",
      fontFamily: "RobotoCondensed-Bold",
      padding: "1em",
      [theme.breakpoints.up("lg")]: {
        fontSize: "2.5em",
      },
      ["@media (max-width:767px)"]: {
        fontSize: "1em",
        lineHeight: "1.5em",
      },
      ["@media (max-width:360px)"]: {
        fontSize: "0.7em",
        lineHeight: "1em",
        paddingTop: "0.6em",
        //backgroundColor: 'red',
      },
    },
    genreCell: {},
    genre: {
      fontFamily: "RobotoCondensed-Light",
      paddingLeft: "2em",
      textTransform: "uppercase",
      ["@media (max-width:475px)"]: {
        fontSize: "0.5em",
        lineHeight: "1em",
      },
      [theme.breakpoints.between("sm", "md")]: {
        fontSize: "1em",
        lineHeight: "1em",
      },
    },
    hosts: {
      fontSize: "0.6m",
      ["@media (max-width:767px)"]: {
        fontSize: "0.8em",
        lineHeight: "1em",
      },
      ["@media (max-width:475px)"]: {
        fontSize: "0.6em",
        lineHeight: "1em",
      },
    },
    timeText: {
      fontFamily: "RobotoCondensed-Light",
      color: "white !important",
      fontSize: "1.2em",
      ["@media (max-width:600px)"]: {
        fontSize: "1em",
      },
    },
    time: {
      paddingLeft: "1em",
      paddingRight: "1em",
      backgroundColor: "rgba(48, 65, 140, 1)",
    },
    synopsisCell: {
      fontFamily: "RobotoCondensed-Bold",
      verticalAlign: "middle",
      ["@media (max-width:475px)"]: {
        paddingLeft: "0em !important",
      },
    },
    synopsis: {
      //paddingRight:"1em",
      fontFamily: "RobotoCondensed-Light",
      fontSize: "1.2em",
      verticalAlign: "middle",
      textAlign: "justify",
      ["@media (max-width:475px)"]: {
        paddingLeft: "0em !important",
        fontSize: "0.6em",
        textAlign: "left",
      },
    },
    openHeader: {
      //paddingLeft:"1em",
      fontFamily: "RobotoCondensed-Bold",
      textTransform: "uppercase",
      paddingRight: "1em",
      fontSize: "1.5em",
      color: "rgba(48, 65, 140, 1)",
      ["@media (max-width:767px)"]: {
        fontSize: "0.6em",
      },
      ["@media (max-width:475px)"]: {
        lineHeight: "1.1em",
      },
    },
    otherSchedulesHeader: {
      //paddingLeft:"1em",
      fontFamily: "RobotoCondensed-Bold",
      textTransform: "uppercase",
      textAlign: "center",
      paddingRight: "1em",
      fontSize: "1.5em",
      color: "rgba(48, 65, 140, 1)",
      ["@media (max-width:767px)"]: {
        fontSize: "0.6em",
      },
      ["@media (max-width:475px)"]: {
        lineHeight: "1.1em",
      },
    },
    classHeader: {
      //paddingLeft:"1em",
      //paddingRight:"1em",
      // fontSize: "1.5em",
      ["@media (max-width:475px)"]: {
        visibility: "hidden",
      },
    },
    classification: {
      fontFamily: "RobotoCondensed-Light",
      verticalAlign: "middle",
      fontSize: "1em",
      ["@media (max-width:475px)"]: {
        fontSize: "0.7em",
      },
    },
  }));

  const classes = useRowStyles();
  const weekDayParam: string = props.weekday;

  return (
    <React.Fragment>
      <TableRow
        className={classes.root}
        onClick={() => {
          setOpen(!open);
        }}
      >
        <Hidden xsDown>
          <TableCell width="20%" className={classes.miniBanner}>
            <MiniBannerComponent showSlug={row.showSlug}></MiniBannerComponent>
          </TableCell>
        </Hidden>
        <TableCell width="10%" className={classes.time} align="center">
          <Typography className={classes.timeText}>
            &nbsp;
            {utils.formatTime(row.startTime)}
            &nbsp;
          </Typography>
        </TableCell>
        <TableCell width="45%" className={classes.showTitleCell} align="left">
          <Typography
            className={classes.showTitle}
            display="block"
            component="div"
          >
            {row.name}
          </Typography>
          {/*} <Typography display="inline" className={classes.hosts} gutterBottom component="div">
              {row.host_1}
            </Typography>
              {row.host_2 !== null  &&
                <Typography display="inline" className={classes.hosts} gutterBottom component="div">
                , {row.host_2}
              </Typography>
              }
              {row.host_3 !== null &&
              <Typography display="inline" className={classes.hosts} gutterBottom component="div">
              ,  {row.host_3}
            </Typography>
            }
            {row.host_4 !== null &&
              <Typography display="inline" className={classes.hosts} gutterBottom component="div">
                , {row.host_4}
            </Typography>*/}
        </TableCell>
        <TableCell width="5%" size="small" align="center">
          <Typography className={classes.genre}>{row.genre}</Typography>
        </TableCell>
        <TableCell width="5%" className={classes.button}>
          <IconButton
            aria-label="expand row"
            size="small"
            onClick={() => {
              setOpen(!open);
            }}
          >
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell
          colSpan={6}
          style={{ paddingTop: "0px", paddingBottom: "0px" }}
        >
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box margin={1}>
              <Table size="small" aria-label="purchases">
                <TableHead className={classes.otherRowHeader}>
                  <TableRow className={classes.otherRowHeader}>
                    <TableCell width="70%" className={classes.openHeader}>
                      Sinopsis
                    </TableCell>
                    <TableCell
                      width="10%"
                      className={classes.openHeader}
                      size="small"
                    >
                      Clasificación
                    </TableCell>
                    <TableCell
                      width="20%"
                      className={classes.otherSchedulesHeader}
                    >
                      {"Otros\n Horarios"}
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  <TableRow>
                    <TableCell
                      width="70%"
                      align="left"
                      className={classes.synopsisCell}
                    >
                      <Typography className={classes.synopsis} align="left">
                        {row.synopsis}
                      </Typography>
                    </TableCell>
                    <TableCell
                      width="10%"
                      align="left"
                      className={classes.classification}
                      size="small"
                    >
                      {row.pgClassification}
                    </TableCell>
                    <TableCell width="20%" align="left">
                      <OtherSchedules
                        otherSchedules={row.showSchedule}
                        weekday={weekDayParam}
                      ></OtherSchedules>
                    </TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
};

export default Row;
