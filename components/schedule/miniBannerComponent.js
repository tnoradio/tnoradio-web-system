import React, { useState } from "react";
import { connect, useDispatch } from "react-redux";
import Image from "react-bootstrap/Image";
import { fetchShowMiniBanner } from "../../redux/actions/shows/fetchShowMiniBanner";
import { useRouter } from "next/router";

const MiniBannerComponent = (props) => {
  const dispatch = useDispatch();
  const router = useRouter();
  const [miniBannerUrl, setMiniBannerUrl] = useState("");
  var data;
  dispatch(fetchShowMiniBanner("showMiniBanner", props.showSlug));

  React.useEffect(() => {
    if (props.showMiniBanner !== undefined) {
      if (
        props.showMiniBanner.url !== null &&
        props.showMiniBanner.url !== undefined &&
        props.showMiniBanner.url !== ""
      ) {
        setMiniBannerUrl(props.showMiniBanner.url);
      } else {
        if (props.showMiniBanner.file?.data !== undefined) {
          data = Buffer.from(props.showMiniBanner.file.data, "binary").toString(
            "base64"
          );
          setMiniBannerUrl(`data:image/jpeg;base64,${data}`);
        }
      }
    }
  }, [props.showMiniBanner]);

  return (
    <Image
      src={miniBannerUrl}
      alt={props?.showSlug}
      width={"100%"}
      height={"100%"}
      onClick={(e) => {
        e.preventDefault();
        router.push("/"+props?.showSlug);
      }}
      fluid
    />
  );
};

const mapStateToProps = (state, ownProps) => {
  return {
    showMiniBanner: state.showApp.miniBanners.find(
      (miniBanner) => miniBanner.showSlug === ownProps.showSlug
    ),
  };
};

export default connect(mapStateToProps, null)(MiniBannerComponent);
