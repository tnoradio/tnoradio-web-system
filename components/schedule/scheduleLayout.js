import React from 'react'
import ScheduleNavbar from './scheduleNavbar'

/**
 * This component will be shared across all pages.
 */

export default function ScheduleLayout({ children }) {
  return (
    <div>
      <ScheduleNavbar></ScheduleNavbar>
      <div style={{minHeight:"30vh"}}>
        {children}   
      </div> 
    </div>       
  )     
}