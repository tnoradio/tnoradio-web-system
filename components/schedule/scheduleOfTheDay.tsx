import React, { useEffect, useState } from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableContainer from "@material-ui/core/TableContainer";
import Paper from "@material-ui/core/Paper";
import { Show } from "../../core/shows/domain/Show";
import { IShowScheduleRow } from "../../interfaces/IShowScheduleRow";
import { useStore } from "../../redux/storeConfig/store";
import utils from "../../core/shows/domain/utils";
import Row from "./showRow";
import { Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import CircularProgress from "@material-ui/core/CircularProgress";

const useStyles = makeStyles((theme) => ({
  root: {
    background: "white",
  },
  container: {
    display: "flex",
  },

  progress: {
    height: "50vh",
  },
}));

/**
 * Component, renders the shows schedule of a day.
 */
var dayShows;
const ScheduleOfTheDay = (props) => {
  const classes = useStyles();
  let store = useStore();
  let dataRows: IShowScheduleRow[] = []; //Data to display in show row
  let weekDayParam = props.weekday;
  const [showsOfTheDay, setShowsOfTheDay] = useState(dataRows);
  if (props.weekday === "miércoles") weekDayParam = "miercoles";
  if (props.weekday === "sabado" || props.weekday === "sábado")
    weekDayParam = "sabado";

  useEffect(() => {
    dataRows = [];
    getShowsOfTheDay();
  }, [props]);

  const getShowsOfTheDay = () => {
    let shows: Show[] = props.showsOnWeekdayList;
    let row: IShowScheduleRow;
    let scheduleIds: Number[] = []; // aux to store ids
    shows.forEach(
      //TODO HACER LA FILA CON EL INITTIME DIRECTO
      (show) => {
        show.showSchedules.forEach((schedule) => {
          if (
            scheduleIds.includes(schedule.id) === false &&
            schedule.weekDay === weekDayParam
          ) {
            row = new IShowScheduleRow(
              show.id,
              show.name,
              show.genre,
              show.type,
              show.synopsis,
              show.pgClassification,
              show.hosts,
              schedule.startTime,
              show.showSchedules,
              show.showSlug
            );
            scheduleIds.push(schedule.id);
            dataRows.push(row);
          }
        });
      }
    );

    dataRows = utils.sortSchedule(dataRows);
    setShowsOfTheDay((showsOfTheDay) => (showsOfTheDay = dataRows));
  };

  if (store.getState().showApp.loadScheduleState === "loaded") {
    dayShows = null;
    return (
      <TableContainer component={Paper}>
        <Table aria-label="collapsible table">
          <TableBody>
            {showsOfTheDay.map((row: IShowScheduleRow) => {
              return (
                <Row
                  key={row.id.toString() + row.startTime.toString()}
                  row={row}
                  weekday={weekDayParam}
                />
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
    );
  } else {
    if (store.getState().showApp.loadScheduleState === "loading") {
      return (
        <Grid
          className={classes.progress}
          container
          alignItems="center"
          justifyContent="center"
        >
          <CircularProgress color="secondary" />
        </Grid>
      );
    }
  }
};

export default ScheduleOfTheDay;
