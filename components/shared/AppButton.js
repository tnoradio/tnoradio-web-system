import Button from '@material-ui/core/Button';
import useStylesFunction from '../shared/MatUIStyles/useStyles.js'
import formStylesFunction from '../shared/MatUIStyles/formStyles.js'

const useStyles = useStylesFunction;
function AppButton (props) {  
  const classes = useStyles();
  return (
    <>
      <Button
        type={props.type}
        fullWidth
        variant="contained"
        color="primary"
        className={classes.submit}
        onClick={props.onClick}>
          {props.text}
      </Button>
    </>
  );
}
  
export default AppButton;