import React from "react";

import formStylesFunction from "../../components/shared/MatUIStyles/formStyles.js";
import useStylesFunction from "../../components/shared/MatUIStyles/useStyles.js";

const formStyles = formStylesFunction;
const useStyles = useStylesFunction;

export default function Logo(props) {
  const classes = useStyles();
  const formClasses = formStyles();

  if (props.form)
    return (
      <img
        style={{ height: "80%", width: "80%" }}
        src="/Logo_TNO.png"
        alt="TNORadio.com"
      />
    );
  else return <img src="/" alt="TNORadio.com" />;
}
