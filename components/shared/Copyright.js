import Typography from "@material-ui/core/Typography";
import Link from "@material-ui/core/Link";

function CopyRight(props) {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="https://material-ui.com/" underline="none">
        TNO Radio. Todos los derechos reservados.
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

export default CopyRight;
