import { makeStyles } from "@material-ui/core/styles";

const formStylesFunction = makeStyles((theme) => ({
  logo: {
    margin: theme.spacing(1),
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
}));

export default formStylesFunction;
