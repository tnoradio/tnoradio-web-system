import { makeStyles } from '@material-ui/core/styles';

const globalStylesFunction = makeStyles((theme) => ({
    logo: {
        margin: theme.spacing(1),
      //  display: 'flex',
        direction: 'column',
        height: '12vh',
        width: '5vh'
    }
  }));

  export default globalStylesFunction;