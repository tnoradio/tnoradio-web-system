import React from "react";
import Box from "@material-ui/core/Box";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";

const Social = () => {
  const useStyles = makeStyles((theme) => ({
    social: {
    },
    socialBox: {
      color: "white",
      fontSize: "2em",
      paddingTop: "1rem",
      paddingBottom: "1rem",
    },
    facebook: {
      backgroundColor: "#3B5998",
    },
    typo: {
      color: "white",
    },
    twitter: {
      backgroundColor: "#1dcaff",
      color: "white",
    },
    x: {
      backgroundColor: "black",
      color: "white",
    },
    linkedin: {
      backgroundColor: "#0E76A8",
      color: "white",
    },
    youtube: {
      backgroundColor: "red",
      color: "white",
    },
    tuneIn: {
      backgroundColor: "#2cb6a7",
      color: "white",
    },
    tuneInLogo: {
      height: "1em",
      [theme.breakpoints.between("xs", "sm")]: {
        height: "1em",
      },
      [theme.breakpoints.between("sm", "md")]: {
        height: "1em",
      },
    },
    tikTok: {
      background: "black",
    },
    instagram: {
      //background: "rgb(254,218,117)",
      background:
        "linear-gradient(90deg, rgba(254,218,117,1), rgba(250,126,30,1), rgba(214,41,118,1), rgba(150,47,191,1), rgba(79,91,213,1))",
    },
  }));

  const classes = useStyles();

  return (
    <>
      <Grid container className={classes.social}>
      <Grid item className={classes.instagram} xs={4}  md={2}>
          <Link
            target="_blank"
            rel="noopener"
            href="https://www.instagram.com/tnoradio"
            underline="none"
          >
            <Box
              className={classes.socialBox}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <span className="fab fa-instagram" />
            </Box>
          </Link>
        </Grid>
        <Grid item className={classes.facebook} xs={4}  md={2}>
          <Link
            target="_blank"
            rel="noopener"
            href="https://www.facebook.com/tnoradio"
            underline="none"
          >
            <Box
              className={classes.socialBox}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <span className="fab fa-facebook" />
            </Box>
          </Link>
        </Grid>
        <Grid item className={classes.linkedin} xs={4}  md={2}>
          <Link
            target="_blank"
            rel="noopener"
            href="https://www.linkedin.com/company/tno-radio"
            underline="none"
          >
            <Box
              className={classes.socialBox}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <span className="fab fa-linkedin"  />
            </Box>
          </Link>
        </Grid>
        <Grid
          item
          container
          className={classes.tikTok}
          xs={4}
          md={2}
          justifyContent="center"
          alignItems="center"
        >
          <Link
            className={classes.link}
            target="_blank"
            rel="noopener"
            href="https://tiktok.com/@tnoradio"
            underline="none"
          >
            <Box
              className={classes.socialBox}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <i className="fa-brands fa-tiktok"></i>
            </Box>
          </Link>
        </Grid>
        <Grid
          item
          container
          className={classes.youtube}
          xs={4}          
          md={2}
          justifyContent="center"
          alignItems="center"
        >
          <Link
            className={classes.link}
            target="_blank"
            rel="noopener"
            href="https://www.youtube.com/c/tnoradio"
            underline="none"
          >
            <Box
              className={classes.socialBox}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <span className="fab fa-youtube" />
            </Box>
          </Link>
        </Grid>
        <Grid item className={classes.x} xs={4}  md={2}>
          <Link
            target="_blank"
            rel="noopener"
            href="https://www.x.com/tnoradio"
            underline="none"
          >
            <Box
              className={classes.socialBox}
              display="flex"
              justifyContent="center"
              alignItems="center"
            >
              <span className="fa-brands fa-x-twitter" />
            </Box>
          </Link>
        </Grid>
      </Grid>
    </>
  );
};

export default Social;
