import Footer from "../components/footer";
import { makeStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import { useRouter } from "next/router";
import { useSelector } from "react-redux";
import React, { useState } from "react";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
} from "reactstrap";

const useStyles = makeStyles((theme) => ({
  link: {
    textDecoration: "none",
    fontFamily: "RobotoCondensed-Light",
    fontSize: "1.2rem",
    color: theme.palette.text.primary,
    "&:hover": {
      cursor: "pointer",
    },
  },
  brand: {
    [theme.breakpoints.down("sm")]: {
      width: "20%",
    },
  },
  logo: {
    width: "13rem",
    //paddingLeft:"1rem",
    [theme.breakpoints.down("sm")]: {
      marginLeft: "0",
      width: "8rem",
    },
  },
  nav: {
    padding: "0",
    justifyContent: "right",
    color: "white",
  },
  navbar: {
    zIndex: "900",
    paddingLeft: "1rem",
    paddingTop: "0rem",
    paddingBottom: "0rem",
    paddingRight: "2rem",
    background:
      "linear-gradient(to left, rgba(11, 135, 147, 1), rgba(48, 65, 140, 1))",
    [theme.breakpoints.down("sm")]: {
      paddingTop: "0rem",
    },
  },
  navlink: {
    color: "white",
  },
  pages: {},
}));

/**
 * This component will be shared across all pages.
 */

export default function Layout({ children }) {
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);
  const classes = useStyles();
  const router = useRouter();
  const state = useSelector((state) => state);

  return (
    <>
      <Navbar color="dark" dark expand="md" className={classes.navbar}>
        <NavbarBrand className={classes.brand} href="/">
          <img className={classes.logo} src="/Logo_TNO_2021_white.png" />
        </NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="ml-auto" navbar>
            <NavItem>
              <NavLink
                className={classes.link}
                style={{
                  color: "red",
                  fontFamily: "RobotoCondensed-Bold",
                }}
                onClick={() => {
                  router.push(
                    {
                      pathname: "/",
                    },
                    undefined,
                    { shallow: true }
                  );
                }}
              >
                EN VIVO
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classes.link}
                onClick={() => {
                  router.push(
                    {
                      pathname: "/programacion/" + state.utilsApp.today,
                    },
                    undefined,
                    { shallow: true }
                  );
                }}
              >
                PROGRAMACIÓN
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classes.link}
                onClick={() => {
                  router.push(
                    {
                      pathname: "/talentos/",
                    },
                    undefined,
                    { shallow: true }
                  );
                }}
              >
                TALENTOS
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classes.link}
                onClick={() => {
                  router.push(
                    {
                      pathname: "/contenido/posts",
                    },
                    undefined,
                    { shallow: true }
                  );
                }}
              >
                CONTENIDO
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classes.link}
                target="_blank"
                onClick={() => {
                  router.push(
                    {
                      pathname: "https://sistema.tnoradio.com",
                    },
                    undefined,
                    { shallow: true }
                  );
                }}
              >
                SOLICITUDES
              </NavLink>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>
      <div className={classes.pages}>{children}</div>
      <Footer></Footer>
    </>
  );
}

Navbar.propTypes = {
  light: PropTypes.bool,
  dark: PropTypes.bool,
  fixed: PropTypes.string,
  color: PropTypes.string,
  role: PropTypes.string,
  expand: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
  tag: PropTypes.oneOfType([PropTypes.func, PropTypes.string]),
  // pass in custom element to use
};
