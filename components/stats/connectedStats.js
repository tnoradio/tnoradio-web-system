import MinimalStatisticsChart2 from "./minimalStatisticsWithChart2Card"
import { StaticCardData } from "../../core/analytics/staticCardData"
import * as Icon from "react-feather"

const ConnectedStats = () =>{
    return (
        <MinimalStatisticsChart2
            chartData={StaticCardData.ChartistData2}
            statistics="2156"
            statisticsColor="#fff"
            text="Usuarios Conectados"
            iconSide="right" >
        <Icon.Activity size={36} strokeWidth="1.3" color="#fff" />
        </MinimalStatisticsChart2>
    );
}

export default ConnectedStats;