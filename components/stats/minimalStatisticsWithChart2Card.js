import React, { PureComponent } from "react";
import { Card, CardBody, Media } from "reactstrap";
import PropTypes from "prop-types";
import classnames from "classnames";

//Chsrtis JS 
import ChartistGraph from "react-chartist";
//Chsrtis CSS


class MinimalStatisticsChart2 extends PureComponent {
   render() {
      let iconLeft;
      let iconRight;
      let textDirection;

      if (this.props.iconSide === "right") {
         iconRight = this.props.children;
      } else {
         iconLeft = this.props.children;
         textDirection = "text-right";
      }
      return (
         <Card style={{ backgroundColor: 'rgba(48, 65, 140, 1)', borderRadius:"0px", border:"none" }}>
            <CardBody className="pt-2 pb-0" style={{borderRadius:"0 !important" }}>
               <Media>
                  {iconLeft}
                  <Media body className={classnames("white", textDirection)}>
                     <h3 style={{color:"white"}}>{this.props.statistics}</h3>
                     <span  style={{color:"white"}}>{this.props.text}</span>
                  </Media>
                  {iconRight}
               </Media>
            </CardBody>
            <ChartistGraph
               className="WidgetlineChart WidgetlineChartshadow mb-3"
               data={this.props.chartData}
               type="Line"
               options={{
                  height: "2em",
                  axisX: {
                     showGrid: false,
                     showLabel: false,
                     offset: 0
                  },
                  axisY: {
                     showGrid: false,
                     low: 50,
                     showLabel: false,
                     offset: 0
                  },
                  fullWidth: true
               }}
            />
         </Card>
      );
   }
}

MinimalStatisticsChart2.propTypes = {
   iconSide: PropTypes.string,   
   statisticsColor: PropTypes.string,
   statistics: PropTypes.string,
   cardBgColor: PropTypes.string,
   text: PropTypes.string,
   chartData: PropTypes.object
};

export default MinimalStatisticsChart2;
