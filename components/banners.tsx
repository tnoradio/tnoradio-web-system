import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import { IBannerImage } from "../interfaces/IBannerImage";
import { makeStyles } from "@material-ui/core/styles";
import { useRouter } from "next/router";
import { useSelector, useDispatch } from "react-redux";
import { fetchShowBanners } from "../redux/actions/shows/fetchShowBanners";
import React, { useEffect } from "react";
import { useTrackEvent } from "../hooks/useTrackEvent";
import useCheckMobileScreen from "../hooks/useCheckMobileScreen"
import Image from 'next/image';

const useStyles = makeStyles(() => ({
  bigGallery: {
    position: "relative",
    width: "100%", 
    height: "100%",
    '& ul': {
      height:"100%",
    },
  },
}));

let images: IBannerImage[] = [];
const Banners = () => {
  const { trackEvent } = useTrackEvent();
  const router = useRouter();
  const state = useSelector((state: any) => state);
  const classes = useStyles();
  const isMobile = useCheckMobileScreen();
  const dispatch = useDispatch();

  useEffect(() => {
    getBanners();
  }, [isMobile]);

  const getBanners = () => {
    if (isMobile === true) dispatch(fetchShowBanners("showResponsiveBanner"));
    if (isMobile === false) dispatch(fetchShowBanners("showBanner"));
  };

  try {
    if (
      isMobile === false &&
      state.showApp.showBanners !== null &&
      state.showApp.showBanners !== undefined
    )
      images = getBannersArray(state.showApp.showBanners);
    if (
      isMobile === true &&
      state.showApp.showResponsiveBanners !== null &&
      state.showApp.showResponsiveBanners !== undefined
    )
      images = getBannersArray(state.showApp.showResponsiveBanners);
  } catch (e) {
    console.log("banners error");
  }

  const responsive = {
    theBiggest: {
      breakpoint: { max: 5000, min: 2601 },
      items: 2,
      slidesToSlide: 1, // optional, default to 1.
    },
    veryBig: {
      breakpoint: { max: 2600, min: 1024 },
      items: 1,
      slidesToSlide: 1, // optional, default to 1.
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 1,
      slidesToSlide: 1, // optional, default to 1.
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
      slidesToSlide: 1, // optional, default to 1.
    },
  };

  const imageLoader = ({ src }) => {
    return `${src}`
  }

  return (
      <Carousel
        arrows={false}
        className={classes.bigGallery}
        swipeable={false}
        draggable={false}
        showDots={false}
        responsive={responsive}
        ssr={true} // means to render carousel on server-side.
        infinite={true}
        autoPlay={true}
        autoPlaySpeed={3000}
        keyBoardControl={true}
        customTransition="all .5"
        transitionDuration={2000}
        removeArrowOnDeviceType={["mobile"]}
        containerClass="carousel-container"
      >
        {images.map((image) => {
          const url = image.slug;
          return (
              <Image
                onClick={() =>{
                  trackEvent(
                    "banner_click",
                    "showBanners",
                    "click",
                    `${image.slug}-Banner`
                  );
                  router.push({pathname: url});
                }}
                key={image.key}
                loader={imageLoader}
                src={image.source}
                alt={`Ir a ${image.slug}`}
                layout='responsive'
                objectFit='contain'
                width={isMobile === false ? 1696 : 424}
                height={isMobile === false ? 254 : 216}
                quality={100}
                style={{cursor: "pointer"}}
              />
          );
        })}
      </Carousel>
  );
};
var data;
let bannerImage;
function getBannersArray(banners): IBannerImage[] {
  let bannersArray: IBannerImage[] = [];
  banners.forEach((image) => {
    if (image.url !== null && image.url !== undefined && image.url !== "") {
      bannerImage = new IBannerImage(
        image.url,
        image.name + image.showSlug,
        image.showSlug
      );
    } else {
      data = Buffer.from(image.file.data, "binary").toString("base64");
      bannerImage = new IBannerImage(
        `data:image/jpeg;base64,${data}`,
        image.name + image.showSlug,
        image.showSlug
      );
    }
    bannersArray.push(bannerImage);
  });

  return bannersArray;
}

export default Banners;
