import React from "react";
import Grid from "@material-ui/core/Grid";
import fetchUserImage from "../redux/actions/users/fetchUserImage";
import { connect } from "react-redux";
import { Card, CardTitle } from "reactstrap";
import { Plus } from "react-feather";
import { Link } from "@material-ui/core";

const UserCard = (props) => {
  const { isTeamPlayer, user } = props;
  const [data, setData] = React.useState("");

  React.useEffect(() => {
    const getUserImage = async () => {
      const imageData = await props.userImage;

      if (imageData.userImage !== undefined && imageData.userImage !== null) {
        var data = Buffer.from(
          imageData.userImage.file.data,
          "binary"
        ).toString("base64");
        setData(data);
      }
    };

    getUserImage();
  }, [data]);

  if (user !== undefined && props !== undefined)
    return (
      <Card className="text-left">
        <div className="card-img">
          {data !== null && data !== "" && data !== undefined ? (
            <img width="100%" src={`data:image/jpeg;base64,${data}`} />
          ) : (
            <img width="100%" src="talent_proportion_tnoradio.jpg" />
          )}
          <CardTitle>{user.name + " " + user.lastName}</CardTitle>
          <Link
            href={
              isTeamPlayer === true
                ? "/equipo/" + user.slug
                : "/talento/" + user.slug
            }
            className="btn btn-floating halfway-fab"
            target="_blank"
            underline="none"
          >
            <Plus size={25} color="#FFF" />
          </Link>
        </div>
      </Card>
    );
  else
    return (
      <div>
        <Layout>
          <Grid
            className={classes.progress}
            container
            alignItems="center"
            justifyContent="center"
          >
            <h1>{"Ha ocurrido un problema."}</h1>
          </Grid>
          <Grid
            className={classes.progress}
            container
            alignItems="center"
            justifyContent="center"
          >
            <Link href={"/" + user.slug} underline="none">
              <h4>{"volver a inicio"}</h4>
            </Link>
          </Grid>
        </Layout>
      </div>
    );
};

const mapStateToProps = (state) => {
  return { userImage: state.userApp.userImage };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return { userImage: dispatch(fetchUserImage("profile", ownProps.user.slug)) };
};

export default connect(mapStateToProps, mapDispatchToProps)(UserCard);
