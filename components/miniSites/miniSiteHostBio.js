import Typography from "@material-ui/core/Typography";
import {
  makeStyles,
  createTheme,
  ThemeProvider,
} from "@material-ui/core/styles";
import { useEffect } from "react";

const MiniSiteHostBio = (props) => {
  const theme = createTheme();
  const useRowStyles = makeStyles((theme) => ({
    name: {
      fontFamily: "RobotoCondensed-Bold",
      paddingLeft: "1rem",
      paddingTop: "1rem",
    },
    bio: {
      fontFamily: "RobotoCondensed-Light",
      paddingLeft: "1rem",
    },
  }));
  const classes = useRowStyles();

  theme.typography.h3 = {
    fontSize: "1.2rem",
    paddingLeft: "1rem",

    "@media (max-width:599px)": {
      fontSize: "2rem",
    },
    "@media (min-width:600px)": {
      fontSize: "1.8rem",
    },
    [theme.breakpoints.up("md")]: {
      fontSize: "2.4rem",
    },
  };

  theme.typography.h4 = {
    fontSize: "1rem",
    paddingLeft: "1rem",

    "@media (max-width:599px)": {
      fontSize: "1.5rem",
    },
    "@media (min-width:600px)": {
      fontSize: "1.2rem",
    },
    [theme.breakpoints.up("md")]: {
      fontSize: "2.0rem",
    },
  };

  theme.typography.body2 = {
    paddingLeft: "1rem",
    [theme.breakpoints.down("sm")]: {
      fontSize: "2rem",
    },
    "@media (min-width:600px)": {
      fontSize: "0.8rem",
    },
    "@media (max-width:800px)": {
      fontSize: "0.6rem",
    },
    [theme.breakpoints.up("md")]: {
      fontSize: "1.1rem",
    },
  };

  const SubTitle = () => {
    let host,
      producer,
      employee = false;

    if (props.roles !== undefined) {
      props.roles.forEach((role) => {
        if (role.role === "HOST") {
          host = true;
        } else {
          if (role.role === "PRODUCER") {
            producer = true;
          } else {
            if (role.role === "EMPLOYEE") {
              employee = true;
            }
          }
        }
      });
    }

    if (host === true)
      return (
        <ThemeProvider theme={theme}>
          <Typography variant="h4">{"Talento TNO Radio"}</Typography>
        </ThemeProvider>
      );
    else if (employee === true)
      return (
        <ThemeProvider theme={theme}>
          <Typography variant="h4">{props.department}</Typography>
        </ThemeProvider>
      );
    else if (producer === true)
      return (
        <ThemeProvider theme={theme}>
          <Typography variant="h4">{"Productor en TNO Radio"}</Typography>
        </ThemeProvider>
      );
    else if (producer === true && host === true)
      return (
        <ThemeProvider theme={theme}>
          <Typography variant="h4">{"Talento en TNO Radio"}</Typography>
        </ThemeProvider>
      );
    else
      return (
        <ThemeProvider theme={theme}>
          <Typography variant="h4">{"TNO Radio"}</Typography>
        </ThemeProvider>
      );
  };

  useEffect(() => {console.log("BIO")}, []);

  return (
    <>
      <ThemeProvider theme={theme}>
        <Typography className={classes.name} variant="h3">
          {props.name}
        </Typography>
        <SubTitle />
        <Typography className={classes.bio} variant="h5" align="left">
          <br></br>
          {props.bio}
        </Typography>
      </ThemeProvider>
    </>
  );
};

export default MiniSiteHostBio;
