import PostSummary from "../../components/posts/postSummary";
import React, { useState, useEffect } from "react";
import { getPostsPageByOwner } from "../../redux/actions/posts/fetchPostsPage";
import Carousel from "react-multi-carousel";

import "react-multi-carousel/lib/styles.css";
const UserPostsList = (props) => {
  let currentOffset = 0;
  const [post, setPost] = useState([]);
  const { userId } = props;

  const responsive = {
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 4,
      slidesToSlide: 4, // optional, default to 1.
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 2,
      slidesToSlide: 2, // optional, default to 1.
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
      slidesToSlide: 1, // optional, default to 1.
    },
  };

  const loadFourPost = async () => {
    var tenPost = [];
    const data = await getPostsPageByOwner(16, currentOffset, userId);
    data.forEach((p) => {
      const index = post.findIndex((object) => object.id === p.id);
      if (index === -1) {
        tenPost.push(p);
        tenPost = tenPost.filter((post) => post.owner_id === userId);
      }
    });
    setPost((post) => [...post, ...tenPost]);
    currentOffset++;
  };

  const handleScroll = (e) => {
    const scrollHeight = e.target.documentElement.scrollHeight;
    const currentHeight = Math.ceil(
      e.target.documentElement.scrollTop + window.innerHeight
    );
    if (currentHeight + 1 >= scrollHeight) {
      loadFourPost();
    }
  };
  useEffect(() => {
    loadFourPost();
    window.addEventListener("scroll", handleScroll);
  }, []);

  const CustomRightArrow = ({ onClick, ...rest }) => {
    const {
      onMove,
      carouselState: { currentSlide, deviceType },
    } = rest;
    // onMove means if dragging or swiping in progress.
    return <button onClick={() => onClick()} />;
  };

  return (
    <div>
      <Carousel
        responsive={responsive}
        autoPlaySpeed={3000}
        afterChange={(previousSlide, { currentSlide, onMove }) => {
          loadFourPost();
        }}
        beforeChange={(nextSlide, { currentSlide, onMove }) => {
          loadFourPost();
        }}
      >
        {post?.map((post) => {
          return (
            <>
              {post.approved && (
                <PostSummary
                  key={"sum" + post._id}
                  title={post.title}
                  subTitle={post.subTitle}
                  image={post.image}
                  tags={post.tags}
                  author={post.owner_id}
                  slug={post.slug}
                  summary={post.summary}
                  text={post.text}
                  showImage={false}
                  isMiniSiteHeight={true}
                ></PostSummary>
              )}
            </>
          );
        })}
      </Carousel>
    </div>
  );
};

export default UserPostsList;
