import Layout from "../layout";
import Typography from "@material-ui/core/Typography";
import React from "react";
import MiniSiteShowBanner from "./miniSiteShowBanner";
import { Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import MiniSiteShowOnDemand from "./miniSiteShowOnDemand";
import "react-multi-carousel/lib/styles.css";
import { useRouter } from "next/router";

const useStyles = makeStyles((theme) => ({
  root: {
    background: "white",
  },
  container: {
    display: "flex",
  },
  onDemand: {
    padding: "20px",
    background: "url(/bkgs/bckg-home-footer.jpg) no-repeat;",
    backgroundPosition: "right",
    [theme.breakpoints.down("sm")]: {
      padding: 0,
    },
  },
  title: {
    fontFamily: "RobotoCondensed-Bold",
    fontSize: "2em",
    [theme.breakpoints.between("xs", "sm")]: {
      fontSize: "2em",
    },
    [theme.breakpoints.between("sm", "md")]: {
      fontSize: "1.5em",
    },
    [theme.breakpoints.between("md", "lg")]: {
      fontSize: "3em",
    },
  },
  noDisplay: {
    fontSize: 50,
    display: "none",
  },
  display: {
    fontSize: 50,
    display: "flex",
    ["@media (max-width:475px)"]: {
      fontSize: 30,
    },
  },
  bigGallery: {
    width: "100%", 
    '& ul': {
      height:"100%",
    },
  },
  showBanner: {
    backgroundColor: "#cd7f9e",
  },
  titleContainer: {
    paddingTop: "1em",
    paddingBottom: "0px !important",
    color: "white",
    ["@media (min-width:600px)"]: {
      paddingTop: ".4em",
    },
  },
  synopsis: {
    background: "linear-gradient(90deg, rgba(65,1,61,1),  rgba(74,46,72,1))",
    paddingLeft: "20px",
    overflow: "hidden",
    "&:hover": {
      cursor: "pointer",
    },
    [theme.breakpoints.down("xs")]: {
      minHeight: "150px",
    },
  },
}));

const ShowMiniSite = (props) => {
  const classes = useStyles();
  const router = useRouter();

  const responsive = {
    all: {
      breakpoint: { max: 4000, min: 0 },
      items: 1
    }
  };

    return (
        <Layout>
          <MiniSiteShowBanner showSlug={"tnobusiness"} />
          <Grid container className={classes.root}>
            {/**end talent grid */}
            <Grid item xs={12} container alignItems="stretch">
              <Grid
                item
                xs={12}
                container
                className={classes.scheduleContainer}
              >
              </Grid>
              {/**End schedule grid */}
              <Grid
                className={`${classes.titleContainer} ${classes.synopsis}`}
                container
                alignItems="flex-start"
                item
                xs={12}
              >
                <Typography className={classes.title}>
                  LOS SECRETOS DETRÁS DE UN EMPRESARIO EXITOSO
                </Typography>
             {/*}   <MiniSiteSynopsis
                  synopsis={"tnobusiness"}
    />*/}
              </Grid>
              {/**end synopsis title */}
              
            </Grid>
            {/**end host1 content */}
            <Grid item xs={12} className={classes.onDemand} container>
              {/*} TAMBIÉN PUEDES VER...*/}
              <MiniSiteShowOnDemand showSlug={"tnobusiness"} />
            </Grid>
          </Grid>
          {/*Minisite container*/}
        </Layout>
    );

};

export default ShowMiniSite;
