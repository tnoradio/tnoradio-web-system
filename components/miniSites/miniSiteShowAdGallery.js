import Carousel from "@brainhubeu/react-carousel";
import "@brainhubeu/react-carousel/lib/style.css";
import { makeStyles } from "@material-ui/core/styles";
import { useEffect, useState } from "react";
import Image from "react-bootstrap/Image";
import { fetchShowAdvertisers } from "../../redux/actions/shows/fetchShowAdvertisers";

const useStyles = makeStyles((theme) => ({
  bigGallery: {
    [theme.breakpoints.down("md")]: {
      display: "none",
    },
  },
  responsiveGallery: {
    display: "none",
    [theme.breakpoints.down("md")]: {
      display: "flex",
    },
  },
  adContainer: {
    paddingRight: "5px",
    paddingTop: "5px",
    paddingBottom: "5px",
    background: "rgba(11, 135, 147, 1)",
  },
}));

const MiniSiteShowAdsGallery = (props) => {
  //let images: IBannerImage[] = [];
  const showId = props.showId;
  const classes = useStyles();
  const [advertisers, setAdvertisers] = useState([]);

  useEffect(() => {
    getAdvertisers();
  }, [showId]);

  const getAdvertisers = async () => {
    const advertisers = await fetchShowAdvertisers(showId);
    setAdvertisers(advertisers);
  }


  return (
    <Carousel
      infinite
      slidesPerPage={3}
      slidesPerScroll={1}
      animationSpeed={1500}
      autoPlay={3000}
      offset={0}
      itemWidth={250}
      stopAutoPlayOnHover
      breakpoints={{
        1920: {
          // these props will be applied when screen width is less than 1000px
          itemWidth: 260,
          slidesPerPage: 6,
          clickToChange: false,
        },
        1500: {
          // these props will be applied when screen width is less than 1000px
          itemWidth: 185,
          slidesPerPage: 6,
          clickToChange: false,
        },

        1400: {
          // these props will be applied when screen width is less than 1000px
          itemWidth: 200,
          slidesPerPage: 6,
          clickToChange: false,
        },
        1300: {
          // these props will be applied when screen width is less than 1000px
          itemWidth: 195,
          slidesPerPage: 6,
          clickToChange: false,
        },
        1200: {
          // these props will be applied when screen width is less than 1000px
          itemWidth: 180,
          slidesPerPage: 6,
          clickToChange: false,
        },
        1100: {
          itemWidth: 165,
          slidesPerPage: 4,
          offset: 0,
        },
        960: {
          itemWidth: 110,
          slidesPerPage: 4,
          offset: 0,
        },
        800: {
          itemWidth: 90,
          slidesPerPage: 4,
          offset: 0,
        },
        700: {
          itemWidth: 100,
          slidesPerPage: 4,
          offset: 0,
        },
        650: {
          itemWidth: 70,
          slidesPerPage: 4,
          offset: 0,
        },
        599: {
          itemWidth: 150,
          slidesPerPage: 4,
          offset: 0,
        },
      }}
    >
      {Array.isArray(advertisers) && advertisers.map((advertiser) => {
        var data = Buffer.from(advertiser.image.data, "binary").toString(
          "base64"
        );
        return (
          <div key={advertiser.id} className={classes.adContainer}>
            <a href={advertiser.website} target="_blank">
              <Image
                src={`data:image/jpeg;base64,${data}`}
                alt={advertiser.name}
                fluid
              />
            </a>
          </div>
        );
      })}
    </Carousel>
  );
};

export default MiniSiteShowAdsGallery;
