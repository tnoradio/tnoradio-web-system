import Layout from "../layout";
import { fetchShowBySlug } from "../../redux/actions/shows/fetchShowBySlug";
import Typography from "@material-ui/core/Typography";
import React, { useEffect } from "react";
import { connect, useDispatch } from "react-redux";
import MiniSiteShowBanner from "./miniSiteShowBanner";
import MiniSiteSynopsis from "./showSynopsis";
import MiniSiteSocial from "./miniSiteSocial";
import MiniSiteShowSchedule from "./minisiteShowSchedule";
import MiniSiteShowAdsGallery from "./miniSiteShowAdGallery";
import { Grid } from "@material-ui/core";
import Collapse from "@material-ui/core/Collapse";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import TalentImage from "./miniSiteHosttImage";
import MiniSiteHostBio from "./miniSiteHostBio";
import CircularProgress from "@material-ui/core/CircularProgress";
import MiniSiteShowOnDemand from "./miniSiteShowOnDemand";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import { useRouter } from "next/router";
import { getUserById } from "../../redux/actions/users/fetchUserById";

const useStyles = makeStyles((theme) => ({
  root: {
    background: "white",
  },
  container: {
    display: "flex",
  },
  progress: {
    height: "50vh",
  },
  onDemand: {
    padding: "20px",
    background: "url(/bkgs/bckg-home-footer.jpg) no-repeat;",
    backgroundPosition: "right",
    [theme.breakpoints.down("sm")]: {
      padding: 0,
    },
  },
  title: {
    fontFamily: "RobotoCondensed-Bold",
    fontSize: "4em",
    [theme.breakpoints.between("xs", "sm")]: {
      fontSize: "2em",
    },
    [theme.breakpoints.between("sm", "md")]: {
      fontSize: "1.5em",
    },
    [theme.breakpoints.between("md", "lg")]: {
      fontSize: "3em",
    },
  },
  noDisplay: {
    fontSize: 50,
    display: "none",
  },
  display: {
    fontSize: 50,
    display: "flex",
    ["@media (max-width:475px)"]: {
      fontSize: 30,
    },
  },
  bigGallery: {
    width: "100%", 
    '& ul': {
      height:"100%",
    },
  },
  showBanner: {
    backgroundColor: "#cd7f9e",
  },
  scheduleContainer: {
    minHeight:"100px",
    background: "rgba(11,135,147) url('/block-time.jpg')", //linear-gradient(to bottom, )) no-repeat",
    backgroundRepeat: "no-repeat",
    backgroundSize: "100%",
    overflow: "hidden",
    backgroundPosition: "bottom",
    [theme.breakpoints.down("xs")]: {
      minHeight: "150px",
    },
    [theme.breakpoints.between("sm", "lg")]: {
      minHeight: "0px",
    },
  },
  titleContainer: {
    paddingTop: "1em",
    paddingBottom: "0px !important",
    // backgroundColor:"blue",
    color: "white",
    ["@media (min-width:600px)"]: {
      paddingTop: ".4em",
    },
  },
  synopsis: {
    background: "linear-gradient(90deg, rgba(65,1,61,1),  rgba(74,46,72,1))",
    paddingLeft: "20px",
    overflow: "hidden",
    "&:hover": {
      cursor: "pointer",
    },
    [theme.breakpoints.down("xs")]: {
      minHeight: "150px",
    },
  },
  talentImage: {
    "&:hover": {
      cursor: "pointer",
    },
  },
  asvertisers: {
    background: "linear-gradient(90deg, rgba(65,1,61,1),  rgba(74,46,72,1))",
  },
  socials: {
    [theme.breakpoints.down("xs")]: {
      height: "180px",
    },
  },
}));

const ShowMiniSite = (props) => {
  const classes = useStyles();
  const router = useRouter();
  const [hosts, setHosts] = React.useState([]);

  function handleHostChange(slug) {
    router.push(`/talento/${slug}`);
  };

  const { showBySlug } = props;

  console.log(showBySlug);

  const getHostById = async (id) => {
    console.log(id)
    return await getUserById(id);
  }


  useEffect(() => {
    setHosts([]); //reset the hosts array
    showBySlug?.hosts?.map(async host => {
      console.log("the host id", host)
      const hostData = await getHostById(host.userId);
      setHosts(hosts => [...hosts, hostData]);
    });
  }, [showBySlug]);


  const responsive = {
    all: {
      breakpoint: { max: 4000, min: 0 },
      items: 1
    }
  };

  const getTwitterUrl = () => {
    var social = showBySlug.socials.find(
      (social) => social.name === "Twitter"
    );
    if (social !== undefined) return social.url + social.userName;
    else return "https://www.twitter.com/tnoradio";
  };
  const getTikTokUrl = () => {
    var social = showBySlug.socials.find(
      (social) => social.name === "TikTok"
    );
    if (social !== undefined) return social.url + social.userName;
    else return "https://www.tiktok.com/@tnoradio";
  };
  const getFacebookUrl = () => {
    var social = showBySlug.socials.find(
      (social) => social.name === "Facebook"
    );
    if (social !== undefined) return social.url + social.userName;
    else return "https://www.facebook.com/tnoradio";
  };
  const getInstagramUrl = () => {
    var social = showBySlug.socials.find(
      (social) => social.name === "Instagram"
    );
    if (social !== undefined) return social.url + social.userName;
    else return "https://www.instagram.com/tnoradio";
  };
  const getSnapchatUrl = () => {
    var social = showBySlug.socials.find(
      (social) => social.name === "Snapchat"
    );
    if (social !== undefined) return social.url;
    else return "";
  };
  const getYoutubeUrl = () => {
    var social = showBySlug.socials.find(
      (social) => social.name === "Youtube"
    );
    if (social !== undefined) return social.url + social.userName;
    else return "https://youtube.com/c/tnoradio";
  };
  const getTuneInUrl = () => {
    var social = showBySlug.socials.find(
      (social) => social.name === "TuneIn"
    );
    if (social !== undefined) return social.url + social.userName;
    else return "https://tunein.com/radio/TNO-RADIO-s236350";
  };
  const getLinkedinUrl = () => {
    var social = showBySlug.socials.find(
      (social) => social.name === "Linkedin"
    );
    if (social !== undefined) return social.url + social.userName;
    else return "https://ve.linkedin.com/company/tno-radio";
  };

  //keep the selected host in the carousel data in state
  const handleHostSelected = (host) => {
    setBio(host.bio);
    setRoles(host.roles);
    setName(host.name);
  };

  if (showBySlug !== null && showBySlug !== "not Found") {
    return (
        <Layout>
          <MiniSiteShowBanner showSlug={showBySlug.showSlug} />
          <Grid container className={classes.root}>
            <Grid
              className={classes.talentImage}
              item
              xs={12}
              sm={4}
              xl={3}
              container
            >
              <Carousel responsive={responsive} className={classes.bigGallery} autoPlay={false}>
                {hosts?.map((host) => {
                  return (
                    <TalentImage
                      key={host.id}
                      talentSlug={host.slug}
                      type="talent"
                      onClick={handleHostChange}
                    />
                  )})}
            </Carousel>
            </Grid>
            {/**end talent grid */}
            <Grid item xs={12} sm={8} xl={9} container alignItems="stretch">
              <Grid
                item
                xs={12}
                container
                className={classes.scheduleContainer}
              >
                {/*} <Grid container alignItems="flex-end" item xs={8}>
                <Typography className = {classes.title2}>
                  HORARIO
                </Typography>
                </Grid>*/}
                <Grid
                  container
                  className={classes.titleContainer}
                  alignItems="flex-start"
                  justifyContent="center"
                  item
                  xs={12}
                >
                  <MiniSiteShowSchedule schedule={showBySlug.showSchedules} />
                </Grid>
              </Grid>
              {/**End schedule grid */}
              <Grid
                className={`${classes.titleContainer} ${classes.synopsis}`}
                container
                alignItems="flex-start"
                item
                xs={12}
              >
                <Typography className={classes.title}>
                  SINOPSIS 
                </Typography>
                <MiniSiteSynopsis
                  synopsis={showBySlug.synopsis}
                />
              </Grid>
              {/**end synopsis title */}

              <Grid
                className={classes.asvertisers}
                container
                alignItems="flex-end"
                item
                xs={12}
              >
                <MiniSiteShowAdsGallery showId={showBySlug.id} />
              </Grid>
              <Grid
                className={classes.socials}
                item
                xs={12}
                container
                alignItems="stretch"
              >
                <MiniSiteSocial
                  twitter={getTwitterUrl()}
                  facebook={getFacebookUrl()}
                  instagram={getInstagramUrl()}
                  linkedin={getLinkedinUrl()}
                  snapchat={getSnapchatUrl()}
                  tuneIn={getTuneInUrl()}
                  youtube={getYoutubeUrl()}
                  tikTok={getTikTokUrl()}
                />
                
              </Grid>
            </Grid>
            {/**end host1 content */}
            <Grid item xs={12} className={classes.onDemand} container>
              {/*} TAMBIÉN PUEDES VER...*/}
              <MiniSiteShowOnDemand showSlug={showBySlug.showSlug} />
            </Grid>
          </Grid>
          {/*Minisite container*/}
        </Layout>
    );
  }
  if (showBySlug === null) {
    return (
      <Layout>
        <Grid
          className={classes.progress}
          container
          alignItems="center"
          justifyContent="center"
        >
          <CircularProgress color="secondary" />
        </Grid>
      </Layout>
    );
  } else if (showBySlug === "not Found")
    return (
      <div>
        <Layout>
          <Grid
            className={classes.progress}
            container
            alignItems="center"
            justifyContent="center"
          >
            <h1>{"El programa que buscas no existe o está mal escrito en la url."}</h1>
          </Grid>
        </Layout>
      </div>
    );
};

ShowMiniSite.getInitialProps = async (ctx) => {
  const { show } = await ctx.store.dispatch(fetchShowBySlug(props.showBySlug));
  return { show };
};

const mapStateToProps = (state) => {
  return { showBySlug: state.showApp.showBySlug };
};

export default connect(mapStateToProps)(ShowMiniSite);
