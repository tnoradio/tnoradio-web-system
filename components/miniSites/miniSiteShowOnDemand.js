import { makeStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import { fetchShowFileNames } from "../../redux/actions/shows/fetchShowsOnDemandData";
import ReactPlayer from "react-player/lazy";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";

const MiniSiteShowOnDemand = (props) => {
  const { showSlug, showFileNames } = props;
  const useStyles = makeStyles((theme) => ({
    player: {
      height: showSlug === "tnobusiness" ? "320px !important" : "260px !important",
      minWidth: showSlug === "tnobusiness" ? "260px !important" : "280px !important",
      width: "100% !important",
      padding: showSlug === "tnobusiness" ? "20px !important" : "10px !important",
    },
    bigGallery: {
      width: "100%", 
      height: "260px !important",
      '& ul': {
        height:showSlug === "100%",
      },
    },
  }));
  
  const classes = useStyles();
  const responsive = {
    superLargeDesktop: {
      // the naming can be any, depends on you.
      breakpoint: { max: 4000, min: 3000 },
      items: 5
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 3
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 2
    },
    mobile: {
      breakpoint: { max: 560, min: 0 },
      items: 1
    }
  };

  return (
    <Carousel
      responsive={responsive}
      className={classes.bigGallery}
    >
        {showFileNames?.map((showFileName) => {
          const fileName = showFileName.File_Name.replace(" ", "%20");
          return (
            <ReactPlayer 
              className={classes.player}
              key={fileName}
              url={`https://ondemand.tnoradio.com/${showSlug}/${fileName}`} 
              controls={true}
            />
        )})}
      </Carousel>
  );
};

const mapStateToProps = (state) => {
  return { showFileNames: state.showApp.showFileNames };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  const { showSlug } = ownProps;
  return { fetchShowFileNames: dispatch(fetchShowFileNames(showSlug)) };
};

export default connect(mapStateToProps, mapDispatchToProps)(MiniSiteShowOnDemand);
