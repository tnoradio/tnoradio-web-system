import { Grid } from '@material-ui/core';
import Image from 'react-bootstrap/Image'

const MiniSiteShowGallery = props => {
    return (
        <>
        <Grid container>
            <Grid item>
                <Image src={props.bannerLink} fluid></Image>
            </Grid>
            <Grid item>
                <Image src={props.bannerLink} fluid></Image>
            </Grid>
            <Grid item>
                <Image src={props.bannerLink} fluid></Image>
            </Grid>
            <Grid item>
                <Image src={props.bannerLink} fluid></Image>
            </Grid>
        </Grid>
        <Grid container>
            <Grid item>
                <Image src={props.bannerLink} fluid></Image>
            </Grid>
            <Grid item>
                <Image src={props.bannerLink} fluid></Image>
            </Grid>
            <Grid item>
                <Image src={props.bannerLink} fluid></Image>
            </Grid>
            <Grid item>
            <Image src={props.bannerLink} fluid></Image>
            </Grid>
        </Grid>
       </>
    )
}

export default MiniSiteShowBanner;