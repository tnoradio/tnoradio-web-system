import Image from "react-bootstrap/Image";
import React from "react";
import fetchUserImage from "../../redux/actions/users/fetchUserImage";
import { connect } from "react-redux";

const TalentImage = ({ onClick, userImage, talentSlug}) => {
  const [data, setData] = React.useState("");

  const getUserImage = async () => {
    const imageData = await userImage;
    if (imageData.userImage !== undefined && imageData.userImage !== null)
      var data = Buffer.from(
        imageData.userImage.file.data,
        "binary"
      ).toString("base64");
    setData(data);
  };

  React.useEffect(() => {
    getUserImage();
  }, [data]);

  if (data !== null && data !== "") {
    return (
      <Image
        style={{ height: "100%" }}
        src={`data:image/jpeg;base64,${data}`}
        fluid
        onClick={() => onClick(talentSlug)}
      />
    );
  } else
    return (
      <Image
        style={{ height: "100%" }}
        src="https://randomuser.me/api/portraits/lego/1.jpg"
        fluid
      />
    );
};

const mapStateToProps = (state) => {
  return { userImage: state.userApp.userImage };
};

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    userImage: dispatch(fetchUserImage(ownProps.type, ownProps.talentSlug)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TalentImage);
