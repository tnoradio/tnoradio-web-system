import Image from 'react-bootstrap/Image'

const MiniSiteRelevantShows = props => {
    return (
        <>
        <Image src={props.bannerLink} fluid></Image>
        </>
    )
}

export default MiniSiteShowBanner;