import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";

const MiniSiteSynopsis = (props) => {
  const useRowStyles = makeStyles((theme) => ({
    synopsis: {
      fontFamily: "RobotoCondensed-Light",
      fontSize: "1.5rem",
      padding: "1rem",
      letterSpacing: "0rem",

      [theme.breakpoints.between("sm", "lg")]: {
        fontSize: "1em",
        paddingTop: "0.5rem",
      },
    },
  }));
  const classes = useRowStyles();
  return (
    <Typography className={classes.synopsis}>
      {props.synopsis}
    </Typography>
  );
};

export default MiniSiteSynopsis;
