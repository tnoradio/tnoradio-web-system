import React from "react";
import Box from "@material-ui/core/Box";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";

const MiniSiteSocial = (props) => {
  const useStyles = makeStyles((theme) => ({
    socialBox: {
      color: "white",
      //height:'2.5em',
      // position:'fixed',
      verticalAlign: "middle",
      fontSize: "3em",

      [theme.breakpoints.between("xs", "sm")]: {
        fontSize: "2em",
      },

      [theme.breakpoints.between("sm", "md")]: {
        fontSize: "1.5em",
      },

      [theme.breakpoints.between("lg", "xl")]: {
        paddingTop: "0.1em",
        paddingBottom: "0.1em",
        fontSize: "2.5em",
      },
    },
    facebook: {
      backgroundColor: "#3B5998",
      //padding:"5.8em",
      color: "white",
      verticalAlign: "middle",
    },
    typo: {
      color: "white",
    },
    twitter: {
      backgroundColor: "#1dcaff",
      color: "white",
    },
    youtube: {
      backgroundColor: "red",
      color: "white",
    },
    tuneIn: {
      backgroundColor: "#2cb6a7",
      color: "white",
    },
    tikTok: {
      backgroundColor: "black",
      color: "white",
    },
    x: {
      backgroundColor: "black",
      color: "white",
    },
    tuneInLogo: {
      height: "1.2em",
      [theme.breakpoints.between("xs", "sm")]: {
        height: "0.7em",
      },
      ["@media (max-width:960px)"]: {
        height: "1em",
      },
    },
    linkedin: {
      backgroundColor: "#0E76A8",
      color: "white",
    },
    instagram: {
      //background: "rgb(254,218,117)",
      background:
        "linear-gradient(90deg, rgba(254,218,117,1), rgba(250,126,30,1), rgba(214,41,118,1), rgba(150,47,191,1), rgba(79,91,213,1))",
    },
    link: {
      verticalAlign: "middle",
    },
  }));

  const classes = useStyles();

  let twitter,
    facebook,
    instagram,
    linkedin,
    youtube,
    tikTok,
    tuneIn = "";

  if (props.twitter) twitter = props.twitter;
  if (props.facebook) facebook = props.facebook;
  if (props.instagram) instagram = props.instagram;
  if (props.linkedin) linkedin = props.linkedin;
  if (props.youtube) youtube = props.youtube;
  if (props.tuneIn) tuneIn = props.tuneIn;
  if (props.tikTok) tikTok = props.tikTok;

  return (
    <>
      <Grid
        item
        container
        className={classes.instagram}
        xs={4}
        sm={2}
        justifyContent="center"
        alignItems="center"
      >
        <Link target="_blank" rel="noopener" href={instagram} underline="none">
          <Box
            className={classes.socialBox}
            display="flex"
            justifyContent="center"
            alignItems="center"
          >
            <span className="fab fa-instagram" />
          </Box>
        </Link>
      </Grid>
      <Grid
        item
        container
        className={classes.x}
        xs={4}
        sm={2}
        justifyContent="center"
        alignItems="center"
      >
        <Link target="_blank" rel="noopener" href={twitter} underline="none">
          <Box
            className={classes.socialBox}
            display="flex"
            justifyContent="center"
            alignItems="center"
          >
            <span className="fa-brands fa-x-twitter"></span>
          </Box>
        </Link>
      </Grid>
      <Grid
        item
        container
        className={classes.facebook}
        xs={4}
        sm={2}
        justifyContent="center"
        alignItems="center"
      >
        <Link
          className={classes.link}
          target="_blank"
          rel="noopener"
          href={facebook}
          underline="none"
        >
          <Box
            className={classes.socialBox}
            display="flex"
            justifyContent="center"
            alignItems="center"
          >
            <span className="fab fa-facebook" />
          </Box>
        </Link>
      </Grid>
      <Grid
        item
        container
        className={classes.youtube}
        xs={4}
        sm={2}
        justifyContent="center"
        alignItems="center"
      >
        <Link
          className={classes.link}
          target="_blank"
          rel="noopener"
          href={youtube}
          underline="none"
        >
          <Box
            className={classes.socialBox}
            display="flex"
            justifyContent="center"
            alignItems="center"
          >
            <span className="fab fa-youtube" />
          </Box>
        </Link>
      </Grid>
      <Grid
        item
        container
        className={classes.tikTok}
        xs={4}
        sm={2}
        justifyContent="center"
        alignItems="center"
      >
        <Link
          className={classes.link}
          target="_blank"
          rel="noopener"
          href={tikTok}
          underline="none"
        >
          <Box
            className={classes.socialBox}
            display="flex"
            justifyContent="center"
            alignItems="center"
          >
            <span className="fa-brands fa-tiktok" />
          </Box>
        </Link>
      </Grid>
      <Grid
        item
        container
        className={classes.linkedin}
        xs={4}
        sm={2}
        justifyContent="center"
        alignItems="center"
      >
        <Link target="_blank" rel="noopener" href={linkedin} underline="none">
          <Box
            className={classes.socialBox}
            display="flex"
            justifyContent="center"
            alignItems="center"
          >
            <span className="fab fa-linkedin" />
          </Box>
        </Link>
      </Grid>
    </>
  );
};

export default MiniSiteSocial;
