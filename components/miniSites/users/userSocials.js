import React from "react";
import Box from "@material-ui/core/Box";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";

const MiniSiteSocial = (props) => {
  const useStyles = makeStyles((theme) => ({
    socialBox: {
      color: "white",
      //height:'2.5em',
      // position:'fixed',
      verticalAlign: "middle",
      ["@media (max-width:600px)"]: {
        fontSize: "2em",
        paddingTop: "0.8em",
        paddingBottom: "0.8em",
        height:'2.5em'
      },
      [theme.breakpoints.up("sm")]: {
        fontSize: "2rem",
      },
      [theme.breakpoints.up("md")]: {
        fontSize: "2.5rem",
      },
    },
    facebook: {
      backgroundColor: "#3B5998",
      //padding:"5.8em",
      color: "white",
      verticalAlign: "middle",
    },
    typo: {
      color: "white",
    },
    twitter: {
      backgroundColor: "#1dcaff",
      color: "white",
    },
    tikTok: {
      backgroundColor: "black",
      color: "white",
    },
    x: {
      backgroundColor: "black",
      color: "white",
    },
    linkedin: {
      backgroundColor: "#0E76A8",
      color: "white",
    },
    instagram: {
      //background: "rgb(254,218,117)",
      background:
        "linear-gradient(90deg, rgba(254,218,117,1), rgba(250,126,30,1), rgba(214,41,118,1), rgba(150,47,191,1), rgba(79,91,213,1))",
    },
    link: {
      verticalAlign: "middle",
    },
  }));

  const classes = useStyles();

  let twitter,
    facebook,
    instagram,
    tikTok,
    linkedin = "";

  if (props.twitter) twitter = props.twitter;
  if (props.facebook) facebook = props.facebook;
  if (props.instagram) instagram = props.instagram;
  if (props.linkedin) linkedin = props.linkedin;
  if (props.tikTok) tikTok = props.tikTok;

  return (
    <>
      <Grid
        item
        container
        className={classes.x}
        xs={3}
        justifyContent="center"
        alignItems="center"
      >
        <Link target="_blank" rel="noopener" href={twitter} underline="none">
          <Box
            className={classes.socialBox}
            display="flex"
            justifyContent="center"
            alignItems="center"
          >
            <span className="fa-brands fa-x-twitter"></span>
          </Box>
        </Link>
      </Grid>
      <Grid
        item
        container
        className={classes.facebook}
        xs={3}
        justifyContent="center"
        alignItems="center"
      >
        <Link
          className={classes.link}
          target="_blank"
          rel="noopener"
          href={facebook}
          underline="none"
        >
          <Box
            className={classes.socialBox}
            display="flex"
            justifyContent="center"
            alignItems="center"
          >
            <span className="fab fa-facebook" />
          </Box>
        </Link>
      </Grid>
      <Grid
        item
        container
        className={classes.instagram}
        xs={3}
        justifyContent="center"
        alignItems="center"
      >
        <Link target="_blank" rel="noopener" href={instagram} underline="none">
          <Box
            className={classes.socialBox}
            display="flex"
            justifyContent="center"
            alignItems="center"
          >
            <span className="fab fa-instagram" />
          </Box>
        </Link>
      </Grid>
  {/*    <Grid
        item
        container
        className={classes.linkedin}
        xs={3}
        justifyContent="center"
        alignItems="center"
      >
        <Link target="_blank" rel="noopener" href={linkedin} underline="none">
          <Box
            className={classes.socialBox}
            display="flex"
            justifyContent="center"
            alignItems="center"
          >
            <span className="fab fa-linkedin" />
          </Box>
        </Link>
  </Grid>*/}
      <Grid
        item
        container
        className={classes.tikTok}
        xs={3}
        justifyContent="center"
        alignItems="center"
      >
        <Link target="_blank" rel="noopener" href={tikTok} underline="none">
          <Box
            className={classes.socialBox}
            display="flex"
            justifyContent="center"
            alignItems="center"
          >
            <span className="fa-brands fa-tiktok" />
          </Box>
        </Link>
      </Grid>
    </>
  );
};

export default MiniSiteSocial;
