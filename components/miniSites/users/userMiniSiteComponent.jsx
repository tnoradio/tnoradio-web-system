import Layout from "../../layout";
import React from "react";
import { connect } from "react-redux";
import MiniSiteSocial from "./userSocials";
import { Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import TalentImage from "../miniSiteHosttImage";
import MiniSiteHostBio from "../miniSiteHostBio";
import CircularProgress from "@material-ui/core/CircularProgress";
import Typography from "@material-ui/core/Typography";
import UserPostsList from "../miniSiteUserPosts";
import { userAgent } from "next/server";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: "white",
  },
  container: {
    display: "flex",
    backgroundColor: "white",
  },
  paper: {
    margin: theme.spacing(2),
    ["@media (max-width:800px)"]: {
      margin: theme.spacing(2),
    },
  },
  title: {
    fontFamily: "RobotoCondensed-Bold",
    fontSize: "2em",
    paddingLeft: "1rem",
    paddingTop: "0.5rem",
    paddingBottom: "0.5rem",
    width: "100%",
    background: "rgba(48, 65, 140, 1)", //linear-gradient(to bottom, )) no-repeat",
    color: "white",
    ["@media (max-width:600px)"]: {
      fontSize: "1em",
      fontWeight: 400,
    },
    [theme.breakpoints.between("sm", "md")]: {
      fontSize: "1.5em",
      fontWeight: 400,
    },
  },
  progress: {
    height: "50vh",
  },
  moreButton: {
    outline: "none !important",
    marginTop: "0.5em",
    "&:hover": {
      color: "white",
    },
  },
  titleContainer: {
    // backgroundColor:"blue",
    color: "white",
    padding: "0",
  },
  talentImage: {
    "&:hover": {
      cursor: "pointer",
    },
  },
}));

const UserMiniSite = (props) => {
  const { userBySlug } = props;
  const classes = useStyles();

  const handleChange = () => {
    setOpen((prev) => !prev);
  };

  const getTwitterUrl = () => {
    var social = userBySlug.socials.find(
      (social) => social.socialNetwork === "Twitter"
    );
    if (social !== undefined) return social.url + social.userName;
    else return "https://www.twitter.com/tnoradio";
  };
  const getTikTokUrl = () => {
    var social = userBySlug.socials.find(
      (social) => social.socialNetwork === "TikTok"
    );
    if (social !== undefined) return social.url + social.userName;
    else return "https://www.tiktok.com/@tnoradio";
  };
  const getFacebookUrl = () => {
    var social = userBySlug.socials.find(
      (social) => social.socialNetwork === "Facebook"
    );
    if (social !== undefined) return social.url + social.userName;
    else return "https://www.facebook.com/tnoradio";
  };
  const getInstagramUrl = () => {
    var social = userBySlug.socials.find(
      (social) => social.socialNetwork === "Instagram"
    );

    if (social !== undefined) return social.url + social.userName;
    else return "https://www.instagram.com/tnoradio";
  };
  const getSnapchatUrl = () => {
    var social = userBySlug.socials.find(
      (social) => social.socialNetwork === "Snapchat"
    );

    if (social !== undefined) return social.url + social.userName;
    else return "";
  };
  const getLinkedinUrl = () => {
    var social = userBySlug.socials.find(
      (social) => social.socialNetwork === "Linkedin"
    );

    if (social !== undefined) return social.url + social.userName;
    else return "https://ve.linkedin.com/company/tno-radio";
  };

  if (userBySlug !== null && userBySlug !== "not Found") {
    return (
      <>
        <Layout>
          <Grid container className={classes.root}>
            <Grid
              className={classes.talentImage}
              item
              xs={12}
              sm={4}
              container
              onClick={handleChange}
            >
              <TalentImage
                type="profile"
                talentSlug={userBySlug.slug}
              />
            </Grid>
            <Grid container item xs={12} sm={8}>
              <Grid item xs={12}>
                <Paper elevation={0} className={classes.paper}>
                  <MiniSiteHostBio
                    name={userBySlug.name + " " + userBySlug.lastName}
                    bio={userBySlug.bio}
                    roles={userBySlug.roles}
                    department={userBySlug.department}
                  ></MiniSiteHostBio>
                </Paper>
              </Grid>
              {/**end host1 content */}
              <Grid container alignItems="flex-end" item xs={12}>
                <Typography className={classes.title}>
                  REDES SOCIALES
                </Typography>
              </Grid>
              {/**end synopsis title */}
              <Grid item xs={12} container alignItems="stretch" style={{minHeight: "50px"}}>
                <MiniSiteSocial
                  twitter={getTwitterUrl()}
                  facebook={getFacebookUrl()}
                  instagram={getInstagramUrl()}
                  linkedin={getLinkedinUrl()}
                  snapchat={getSnapchatUrl()}
                  tikTok={getTikTokUrl()}
                ></MiniSiteSocial>
              </Grid>
            </Grid>
            <Grid item xs={12}>
              <UserPostsList userId={userBySlug._id}></UserPostsList>
            </Grid>
          </Grid>
        </Layout>
      </>
    );
  } else {
    if (userBySlug === null) {
      return (
        <Layout>
          <Grid
            className={classes.progress}
            container
            alignItems="center"
            justifyContent="center"
          >
            <CircularProgress color="secondary" />
          </Grid>
        </Layout>
      );
    } else if (userBySlug === "not Found")
      return (
        <div>
          <Layout>
            <Grid
              className={classes.progress}
              container
              alignItems="center"
              justifyContent="center"
            >
              <h1>{"El talento que buscas no existe o está mal escrito."}</h1>
            </Grid>
          </Layout>
        </div>
      );
  }
};

UserMiniSite.getInitialProps = async (ctx) => {
  const { user } = await ctx.store.dispatch(fetchUserBySlug(props.userBySlug));
  console.log(userAgent)
  return { user };
};

const mapStateToProps = (state) => {
  return {
    userBySlug: state.userApp.userBySlug,
  };
};

export default connect(mapStateToProps)(UserMiniSite);
