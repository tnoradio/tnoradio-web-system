import { Link, makeStyles } from "@material-ui/core";
import { Button, Card } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { fetchPostAuthor } from "../../redux/actions/posts/fetchPostAuthor";
import { useTrackEvent } from "../../hooks/useTrackEvent";

const PostSummary = (props) => {
  const dispatch = useDispatch();
  const state = useSelector((state) => state);

  const {
    title,
    image,
    tags,
    author,
    summary,
    slug,
    showImage,
    isMiniSiteHeight,
  } = props;

  const { trackEvent } = useTrackEvent();
  const useRowStyles = makeStyles((theme) => ({
    title: {
      fontFamily: "RobotoCondensed-Bold",
      color: "rgba(48, 65, 140, 1)",
      fontSize: "1.5rem",
    },
    summary: {
      fontFamily: "RobotoCondensed-Light",
      color: "rgba(11, 135, 147, 1)",
    },
    label: { fontFamily: "RobotoCondensed-Light", fontSize: "0.8rem" },
    card: {
      width: "100%",
      height: "100%",
      minHeight: isMiniSiteHeight === true ? "20rem" : "45rem",
      backgroundColor: "transparent",
    },
  }));
  const classes = useRowStyles();

  useEffect(() => {
    dispatch(fetchPostAuthor(author));
  }, [author]);

  return (
    <Card className={classes.card}>
      {showImage === true && <Card.Img variant="top" src={image} />}
      <Card.Body>
        <Link
          underline="none"
          target="_blank"
          rel="noopener"
          href={`/contenido/${slug}`}
          onClick={() => {
            trackEvent("post_title_click", "posts", "post_title_click", "Post");
          }}
        >
          <Card.Title className={classes.title}>{title}</Card.Title>
        </Link>
        <Card.Text className={classes.summary}>{summary}</Card.Text>
        <Card.Text className={classes.summary}>
          Autor:{" "}
          <Link
            href={
              state.postApp.postAuthors.has(author)
                ? "/equipo/" + state.postApp.postAuthors.get(author).slug
                : "contenido/posts"
            }
            target="_blank"
            underline="none"
          >
            {state.postApp.postAuthors.has(author) &&
              state.postApp.postAuthors.get(author).name}
          </Link>
        </Card.Text>
        <Card.Text>
          <Button className={classes.label} variant="info" size="sm">
            {tags[0]}
          </Button>{" "}
          <Button className={classes.label} variant="success" size="sm">
            {tags[1]}
          </Button>
        </Card.Text>
      </Card.Body>
      {/*  <Card.Footer>
          <Button className={classes.label} variant="info" size="sm">
            {tags[0]}
          </Button>{" "}
          <Button className={classes.label} variant="success" size="sm">
            {tags[1]}
          </Button>
      </Card.Footer>*/}
    </Card>
  );
};

export default PostSummary;
