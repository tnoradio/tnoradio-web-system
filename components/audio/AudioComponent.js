import React, { useState, useEffect } from "react";

import Grid from "@material-ui/core/Grid";

import Typography from "@material-ui/core/Typography";

import AudioPlayerTNO from "./audioPlayer";

import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    ...theme.typography.button,
    fontFamily: "RobotoCondensed-Regular",
    // padding: theme.spacing(1),
  },
  container: {
    backgroundColor: "#61e2ff",
    color: "#0092d4",
    //background: "linear-gradient(to bottom, rgba(11, 135, 147, 1), rgba(48, 65, 140, 1))" ,
    height: "100%",
  },
}));

function AudioComponent() {
  const classes = useStyles();
  return (
    <div>
      <Grid container className={classes.container} justifyContent="center">
        <Grid
          container
          item
          xs={2}
          alignItems="center"
          justifyContent="flex-end"
        >
          <img
            src="/aud.gif"
            style={{ height: "100%", width: "70%", paddingLeft: "0em" }}
          />
        </Grid>
        <Grid container item xs={8} alignItems="center" justifyContent="center">
         <div className={classes.root}>Somos la primera radio visual de Venezuela</div>
        </Grid>
        <Grid
          container
          item
          xs={2}
          justifyContent="flex-start"
          alignItems="center"
        >
           <img
            src="/aud.gif"
            style={{ height: "100%", width: "70%", paddingLeft: "0em" }}
          />
        </Grid>
      </Grid>
    </div>
  );
}

class PlayIcon extends React.Component {
  render() {
    return (
      <div
        className=""
        style={{
          color: "#0092d4",
          backgroundColor: "#0092d4",
          height: "0px",
          width: "0px",
        }}
      >
        {/* <img src="/play-button.gif" style={{height:"0px", width:"0px"}}/>*/}
      </div>
    );
  }
}

export default AudioComponent;
