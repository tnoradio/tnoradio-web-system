import ReactPlayer from "react-player";
import React, { useState } from "react";
import PlayArrowIcon from "@material-ui/icons/PlayArrow";
import PauseIcon from "@material-ui/icons/Pause";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles((theme) => ({
  container: {
    // backgroundColor:" rgba(48, 65, 140, 1)",
    //  background: "linear-gradient(to left, rgba(11, 135, 147, 1), rgba(48, 65, 140, 1))" ,
    height: "100%",
  },
  icon: {
    // color:"#0092d4",
    paddingRight: "0.3em",
    width: "1.8em",
    height: "1.8em",
    "&:hover": {
      color: "white !important",
      cursor: "pointer",
    },
  },
}));

export default function AudioPlayerTNO() {
  const classes = useStyles();
  const [playing, setPlaying] = useState(false);
  const [muted, setMute] = useState(true);

  if (playing) {
    return (
      <div>
        <Grid
          className={classes.container}
          container
          justifyContent="center"
          alignItems="center"
        >
          <ButtonPause classes={classes}></ButtonPause>
          <ReactAudioPlayer></ReactAudioPlayer>
        </Grid>
      </div>
    );
  } else {
    return (
      <Grid
        className={classes.container}
        container
        justifyContent="center"
        alignItems="center"
      >
        <ButtonPlay classes={classes}></ButtonPlay>
        <ReactAudioPlayer></ReactAudioPlayer>
      </Grid>
    );
  }

  function ButtonPause(props) {
    return (
      <PauseIcon
        style={{ color: "#0092d4" }}
        className={props.classes.icon}
        onClick={() => {
          setMute(true);
          setPlaying(false);
        }}
      />
    );
  }

  function ButtonPlay(props) {
    return (
      <PlayArrowIcon
        style={{ color: "#0092d4" }}
        className={props.classes.icon}
        onClick={() => {
          setPlaying(true);
          setMute(false);
        }}
      />
    );
  }

  function ReactAudioPlayer() {
    return (
      <ReactPlayer
        url="https://s1.redradioypc.com:8037/;&type=mp3"
        playing={playing}
        controls={false}
        volume={1}
        muted={muted}
        width="0%"
        height="0%"
        light={false}
        onReady={() => {
          console.log("audioPlayer, audio ready now");
          //changeButton()
          //setPlaying(true);
        }}
        onStart={() => {
          console.log("audioPlayer, audio playing");
        }}
        onPlay={() => {
          console.log("audioPlayer, audio resumed");
        }}
        onPause={() => {
          console.log("audioPlayer, audio paused");
        }}
      />
    );
  }
}
