import React, { useEffect } from "react";
import "@chatscope/chat-ui-kit-styles/dist/default/styles.min.css";

import {
  ChatContainer,
  MessageList,
  Message,
  MessageInput,
  Avatar,
  ConversationHeader,
  TypingIndicator,
} from "@chatscope/chat-ui-kit-react";
import { useDispatch, useSelector } from "react-redux";
import {
  addOutgoingMessage,
  makeTextInputRequest,
} from "../../redux/actions/chats/makeTextInputRequest";
import { useStore } from "../../redux/storeConfig/store";

const ChatBot = () => {
  //const robotIco = "/robotAvatar.png";
  const robotIco = "/bot_tno.jpeg";
  const state = useSelector((state: any) => state);
  const store = useStore();
  const dispatch = useDispatch();
  const [botIsTyping, setBotIsTyping] = React.useState(false);
  const handleSend = (e) => {
    dispatch(addOutgoingMessage(e));
    setBotIsTyping(true);
    dispatch(makeTextInputRequest(e));
    setTimeout(() => setBotIsTyping(false), 1000);
  };

  useEffect(() => {}, [store.getState()]);

  return (
    <div
      style={{
        height: "400px",
        width: "250px",
      }}
    >
      <ChatContainer>
        <ConversationHeader>
          <Avatar src={robotIco} name="BotTNO" />
          <ConversationHeader.Content
            userName="BotTNO"
            info="Siempre en línea"
          />
        </ConversationHeader>
        <MessageList
          typingIndicator={
            botIsTyping && <TypingIndicator content="BotTNO está escribiendo" />
          }
        >
          {state.chatbotApp &&
            state.chatbotApp.map((message) => {
              return (
                <Message
                  model={{
                    message: message.message,
                    sentTime: message.sentTime,
                    direction: message.direction,
                    position: message.position,
                  }}
                />
              );
            })}
        </MessageList>
        <MessageInput
          placeholder="Escribe tu mensaje aquí"
          attachButton={false}
          onSend={handleSend}
        />
      </ChatContainer>
    </div>
  );
};

export default ChatBot;
