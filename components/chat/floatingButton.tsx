import React from "react";
import { Button } from "@material-ui/core";
import FloatingComponent from "react-floating-component";

const FloatingChatBubble = (props) => {
  const { onClick } = props;
  return (
    <div>
      <FloatingComponent>
        <Button onClick={onClick}>
          <span
            className="fab fa-weixin"
            style={{
              color: "white",
              height: "3rem",
              width: "3rem",
              fontSize: "50px",
            }}
          />
        </Button>
      </FloatingComponent>
    </div>
  );
};

export default FloatingChatBubble;
