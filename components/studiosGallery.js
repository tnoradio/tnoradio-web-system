import images from './images'
import Gallery from 'react-grid-gallery'
import PropTypes from 'prop-types'
import { makeStyles } from "@material-ui/core/styles"

const useStyles = makeStyles((theme) => ({
    gallery: {
      width:"100%",
      height:"100%"
    }
}))

export default function StudiosGallery () {
    const classes = useStyles();
    return (
        <div className={classes.gallery}>
            <Gallery images={images} />
        </div>
    )
}

Gallery.propTypes = {
    images: PropTypes.arrayOf(
        PropTypes.shape({
            src: PropTypes.string.isRequired,
            thumbnail: PropTypes.string.isRequired,
            srcset: PropTypes.array,
            caption: PropTypes.oneOfType([
                PropTypes.string,
                PropTypes.element
            ]),
            thumbnailWidth: PropTypes.number.isRequired,
            thumbnailHeight: PropTypes.number.isRequired,
            isSelected: PropTypes.bool
        })
    ).isRequired
};
