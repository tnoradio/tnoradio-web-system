import React, { useState } from "react";
import TextField from "@material-ui/core/TextField";
import formStylesFunction from "../shared/MatUIStyles/formStyles.js";
import AppButton from "../shared/AppButton";
import { useForm } from "react-hook-form";
import { UserRepository } from "../../core/users/domain/UserRepository";
import Validations from "../../helper/validations";
import { AxiosUserRepository } from "../../core/users/infrastructure/repository/AxiosUserRepository";
import { UniqueEmailValidator } from "../../core/users/application/use_cases/validateemail/UniqueEmailValidator";
import { Alert, AlertTitle } from "@material-ui/lab";
import Collapse from "@material-ui/core/Collapse";
import { UserCreator } from "../../core/users/application/use_cases/createuser/UserCreator";
import { useRouter } from "next/router";

/**
 * User form skeleton.
 */
type Inputs = {
  email: string;
  password: string;
  repeatPassword: string;
};

const formStyles = formStylesFunction;
const userRepository: UserRepository = new AxiosUserRepository();

const UserForm = () => {
  const formClasses = formStyles();
  const { register, errors, getValues, handleSubmit } = useForm<Inputs>();
  const [open, openEmailError] = React.useState(false);
  const router = useRouter();
  const isValidEmail = new UniqueEmailValidator(userRepository);
  const createUser = new UserCreator(userRepository);

  const onSubmit = async (data: any) => {
    try {
      var emailIsValid;
      emailIsValid = await isValidEmail.run(data);
      openEmailError(!emailIsValid);

      if (emailIsValid) {
        var user = await createUser.run(data);
        router.push({
          pathname: "sistema.tnoradio.com",
          query: { userRegistered: user.email.value },
        });
      }
    } catch (err) {
      console.log("UserForm ", err);
    }
  };

  return (
    <div>
      <form
        name="userForm"
        className={formClasses.form}
        onSubmit={handleSubmit(onSubmit)}
      >
        <TextField
          variant="outlined"
          required
          margin="normal"
          fullWidth
          id="email"
          label="Correo electrónico"
          name="email"
          autoFocus
          inputRef={register({
            validate: (value) => Validations.isValidEmail(value),
          })}
          error={errors.email ? true : false}
        />
        {errors.email && (
          <div className="error">
            Debe escribir una dirección de correo válida.
          </div>
        )}
        <Collapse in={open}>
          <Alert
            severity="error"
            onClose={() => {
              openEmailError(false);
            }}
          >
            <AlertTitle>Error</AlertTitle>
            Este correo ya se encuentra registrado. —{" "}
            <strong>Por favor regístrese con otro correo!</strong>
          </Alert>
        </Collapse>
        <TextField
          variant="outlined"
          required
          margin="normal"
          fullWidth
          name="password"
          label="Contraseña"
          id="password"
          autoComplete="current-password"
          inputRef={register({
            validate: (value) => Validations.isValidPass(value),
          })}
          error={errors.password ? true : false}
        />
        {errors.password && (
          <div className="error">
            La contraeña debe tener entre 8 y 15 caracteres y al menos una
            mayúscula, una minúscula y un número.
          </div>
        )}
        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          name="repeatPassword"
          label="Repeat Password"
          id="Repetir Contraseña"
          autoComplete="current-password"
          inputRef={register({
            validate: (value) =>
              Validations.passwordsMatch(value, getValues()["password"]),
          })}
          error={errors.repeatPassword ? true : false}
        />
        {errors.repeatPassword && (
          <div className="error">La contraeñas no coinciden.</div>
        )}
        <AppButton text="Enviar" type="submit" />
      </form>
    </div>
  );
};

export default UserForm;
