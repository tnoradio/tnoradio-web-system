import React, { useEffect, useState } from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableContainer from "@material-ui/core/TableContainer";
import Paper from "@material-ui/core/Paper";
import Row from "./hostRow";

/**
 * Component, renders the shows schedule of a day.
 */
const HostsList = (props) => {
  useEffect(() => {}, [props]);

  return (
    <TableContainer component={Paper}>
      <Table aria-label="collapsible table">
        <TableBody>
          {props.talents.map((row) => {
            return <Row key={row._id} row={row} />;
          })}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default HostsList;
