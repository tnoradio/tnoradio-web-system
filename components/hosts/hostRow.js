import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import Typography from "@material-ui/core/Typography";
import { Link } from "@material-ui/core";

/**
 * Receives a  list with the row data and renders a table with
 * show rows.
 * @param props
 */
const Row = (props) => {
  const { row } = props;
  const [open, setOpen] = React.useState(false);

  const useRowStyles = makeStyles({
    otherRowHeader: {
      "& > *": {
        padding: "0px",
      },
    },
    root: {
      "& > *": {
        padding: "0px",
        background: "white",
        color: open ? "white !important" : "rgba(48, 65, 140, 1) !important",
      },
    },
    button: {
      paddingLeft: "0.5em",
      paddingRight: "2em",
      ["@media (max-width:475px)"]: {
        paddingRight: ".5em",
      },
    },
    miniBanner: {
      background: "rgba(48, 65, 140, 1)",
    },
    ["@media (max-width:767px)"]: {
      visibility: "hidden",
    },
    showTitleCell: {
      paddingLeft: ".3em",
      textTransform: "uppercase",
      "&:hover": {
        cursor: "pointer",
        background: "rgba(48, 65, 140, 1)",
        color: "white !important",
        textDecorationLine: "none",
      },
    },
    showTitle: {
      fontSize: "1.7em",
      fontWeight: 500,
      paddingLeft: "2em",
      "&:hover": {
        cursor: "pointer",
        color: "white !important",
        textDecorationLine: "none",
      },
      ["@media (max-width:767px)"]: {
        fontSize: "1em",
        lineHeight: "1.5em",
      },
      ["@media (max-width:360px)"]: {
        fontSize: "0.7em",
        lineHeight: "1em",
        paddingTop: "0.6em",
        //backgroundColor: 'red',
      },
    },

    classHeader: {
      //paddingLeft:"1em",
      //paddingRight:"1em",
      // fontSize: "1.5em",
      ["@media (max-width:475px)"]: {
        visibility: "hidden",
      },
    },
  });

  const myLoader = ({ src, width, quality }) => {
    return `${process.env.NEXT_API_USERS_URL}image/${src}`;
  };

  const getProfileImageUrl = (images) => {
    var image = images.find((image) => image.imageName === "profile");

    return image.imageUrl;
  };

  const classes = useRowStyles();

  return (
    <React.Fragment>
      <TableRow
        className={classes.root}
        onClick={() => {
          setOpen(!open);
        }}
      >
        <Hidden xsDown>
          <TableCell width="5%" className={classes.miniBanner}></TableCell>
          <TableCell width="10%" className={classes.miniBanner}>
            <Link href={"talento/" + row.slug} underline="none">
              <Image
                loader={myLoader}
                src={getProfileImageUrl(row.images)}
                alt={row.name + " " + row.lastName}
                width={200}
                height={200}
              />
            </Link>
          </TableCell>
        </Hidden>
        <TableCell width="70%" className={classes.showTitleCell} align="left">
          <Link href={"talento/" + row.slug} underline="none">
            <Typography
              className={classes.showTitle}
              display="block"
              underline="none"
            >
              {row.name + " " + row.lastName}
            </Typography>
          </Link>
        </TableCell>
        <TableCell width="5%" className={classes.miniBanner}></TableCell>
      </TableRow>
    </React.Fragment>
  );
};

export default Row;
