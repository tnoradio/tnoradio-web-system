import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import { useSelector } from "react-redux";
import PropTypes from "prop-types";
import React, { useEffect } from "react";
import { Show } from "../core/shows/domain/Show";
import Image from 'next/image';

const LiveNow = () => {
  const imageLoader = ({ src, width, quality }) => {
    return `/${src}?w=${width}&q=${quality || 75}`
  }

  let liveNow = false;

  const useStyles = makeStyles((theme) => ({
    image: {
     marginTop: "0px",
     cursor: "pointer"
    },
  }));

  let mini_banner_url = "miniSiteBanner_tnoradio_tnobussiness.webp";
  const state = useSelector((state) => state);

  useEffect(() => {
    if (state.showApp.showOnline !== null && state.showApp.showOnline !== "") {
      mini_banner_url = state.showApp.showOnline.mini_banner_link;
      liveNow = true;
    } else {
      if (
        state.showApp.showOnline === null ||
        state.showApp.showOnline === ""
      ) {
        mini_banner_url = "miniSiteBanner_tnoradio_tnobussiness.webp";
        liveNow = false;
      }
    }
  }, [state]);

  const classes = useStyles();

  return (
    <div className={classes.image}>
       <Image
          loader={imageLoader}
          src="miniSiteBanner_tnoradio_prop.webp"
          alt="Radio visual Venezuela"
          layout='responsive'
          objectFit='fill'
          quality={100}
          height={200}
          width={400}
          priority
          onClick={() => window.open("https://www.tnoradio.com/tnobusiness")}
        />
      </div>
  );
};

LiveNow.prototype = {
  showOnline: PropTypes.objectOf(Show),
};
export default LiveNow;
