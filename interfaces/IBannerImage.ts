export class IBannerImage {
    source: string;
    key: string;
    slug: string;
  
    constructor(source: string, key: string, slug: string) {
        this.source = source;
        this.key = key;
        this.slug = slug;            
    }
}