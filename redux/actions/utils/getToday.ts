import * as types from "../../types/types";

export const getToday = () => (dispatch) => {
  var now = new Date(Date.now());
  var today = now.getDay();

  switch (today) {
    case 0:
      dispatch({ type: types.GET_TODAY, today: "domingo" });
    case 1:
      dispatch({ type: types.GET_TODAY, today: "lunes" });
    case 2:
      dispatch({ type: types.GET_TODAY, today: "martes" });
    case 3:
      dispatch({ type: types.GET_TODAY, today: "miercoles" });
    case 4:
      dispatch({ type: types.GET_TODAY, today: "jueves" });
    case 5:
      dispatch({ type: types.GET_TODAY, today: "viernes" });
    case 6:
      dispatch({ type: types.GET_TODAY, today: "sábado" });
    default:
      dispatch({ type: types.GET_TODAY, today: "lunes" });
  }
};
