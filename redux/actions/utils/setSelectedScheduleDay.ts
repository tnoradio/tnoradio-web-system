import * as types from "../../types/types";

export const setSelectedScheduleDay = (day: string) => (dispatch) => {
  dispatch({ type: types.SET_SELECTED_SCHEDULE_DAY, selectedScheduleDay: day });
};
