import { PostRepository } from "../../../core/posts/domain/PostRepository";
import { AxiosPostRepository } from "../../../core/posts/infrastructure/repository/AxiosPostRepository";
import * as types from "../../types/types";
import Post from "../../../core/posts/domain/Post";
import { AllPostsGetter } from "../../../core/posts/application/use_cases/getPosts/allPostsGetter";

const postRepository: PostRepository = new AxiosPostRepository();
const indexPosts = new AllPostsGetter(postRepository);
var posts: Post[] | Error = [];

export const fetchPosts = () => (dispatch) => {
  indexPosts
    .run()
    .then((data) => {
      posts = data;
      dispatch({ type: types.FETCH_POSTS, posts: data });
    })
    .catch((err) => {
      console.log(err);
      let errorMessage = "No se pudo obtener la lista de posts";
      if (err.data) {
        errorMessage = err.data.error;
      } else {
        if (err.message) {
          errorMessage = err.message;
        } else {
        }
      }
      // toastr.error(errorMessage);
      dispatch({
        type: types.FETCH_POSTS,
        posts: [],
      });
    });
};
