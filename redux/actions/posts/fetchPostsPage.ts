import { PostRepository } from "../../../core/posts/domain/PostRepository";
import { AxiosPostRepository } from "../../../core/posts/infrastructure/repository/AxiosPostRepository";
import * as types from "../../types/types";
import Post from "../../../core/posts/domain/Post";
import { PostsPageGetter } from "../../../core/posts/application/use_cases/getPostsPage/postsPageGetter";
import { PostsPageByOwnerGetter } from "../../../core/posts/application/use_cases/getPostsPageByOwner/postsPageByOwnerGetter";

const postRepository: PostRepository = new AxiosPostRepository();
const indexPostsPage = new PostsPageGetter(postRepository);
const indexPostsPageByOwner = new PostsPageByOwnerGetter(postRepository);
var posts: Post[] | Error = [];

export const fetchPostsPage =
  (pageSize: number, page: number) => (dispatch) => {
    indexPostsPage
      .run(pageSize, page)
      .then((data) => {
        dispatch({
          type: types.FETCH_POSTS_PAGE,
          postsPage: data,
          page: page,
          pageSize: pageSize,
        });
      })
      .catch((err) => {
        console.log("fetchPostsPage ", err);
        let errorMessage = "No se pudo obtener la lista de posts";
        if (err.data) {
          errorMessage = err.data.error;
        } else {
          if (err.message) {
            errorMessage = err.message;
          } else {
          }
        }
        // toastr.error(errorMessage);
        dispatch({
          type: types.FETCH_POSTS_PAGE,
          postsPage: [],
          page: 0,
          pageSize: 0,
        });
      });
  };

export const getPostsPageByOwner = (
  pageSize: number,
  page: number,
  owner: string
) => {
  return indexPostsPageByOwner
    .run(pageSize, page, owner)
    .then((data) => data)
    .catch((err) => {
      console.log(err);
      let errorMessage = "No se pudo obtener la lista de posts";
      if (err.data) {
        errorMessage = err.data.error;
      } else {
        if (err.message) {
          errorMessage = err.message;
        } else {
        }
      }
      // toastr.error(errorMessage);
      return [];
    });
};
export const getPostsPage = (pageSize: number, page: number) => {
  return indexPostsPage
    .run(pageSize, page)
    .then((data) => data)
    .catch((err) => {
      console.log(err);
      let errorMessage = "No se pudo obtener la lista de posts";
      if (err.data) {
        errorMessage = err.data.error;
      } else {
        if (err.message) {
          errorMessage = err.message;
        } else {
        }
      }
      // toastr.error(errorMessage);
      return [];
    });
};
