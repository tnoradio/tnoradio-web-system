import { AxiosPostRepository } from '../../../core/posts/infrastructure/repository/AxiosPostRepository'
import { DetailPostGetter } from '../../../core/posts/application/use_cases/getPost/getPostBySlug'
import { PostRepository } from '../../../core/posts/domain/PostRepository'
import * as types from '../../types/types'

const postRepository: PostRepository = new AxiosPostRepository();
const detailPostGetter: DetailPostGetter = new DetailPostGetter(postRepository);

export const fetchPostBySlug = (slug) => (dispatch) => {
   detailPostGetter
     .run(slug)
     .then((data) => {dispatch({ type: types.FETCH_POST_BY_SLUG, postBySlug: data })})
     .catch((err) => {
       console.log("fetchPostErr", err);
       let errorMessage = "No se pudo obtener la lista de users";
       if (err.data) {
         errorMessage = err.data.error;
       } else {
         if (err.message) {
           errorMessage = err.message;
         } else {
         }
       }
       // toastr.error(errorMessage);
       dispatch({
         type: types.FETCH_POST_BY_SLUG,
         postBySlug: [],
       });
     });
 };