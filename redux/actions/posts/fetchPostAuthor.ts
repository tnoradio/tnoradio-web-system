import { AxiosUserRepository } from "../../../core/users/infrastructure/repository/AxiosUserRepository";
import { UserRepository } from "../../../core/users/domain/UserRepository";
import { User } from "../../../core/users/domain/User";
import * as types from "../../types/types";
import { DetailUserGetterById } from "../../../core/users/application/use_cases/getUserDetailById/detailusergetterbyid";

const userRepository: UserRepository = new AxiosUserRepository();
const userByIdUserGetter = new DetailUserGetterById(userRepository);

export const fetchPostAuthor = (_id) => async (dispatch) => {
  try {
    const author = await userByIdUserGetter.run(_id);
    return dispatch({ type: types.FETCH_POST_AUTHOR, author: author });
  } catch (err) {
    console.log("fetchPostAuthor ", err);
    let errorMessage = "No se pudo obtener la lista de users";
    if (err.data) {
      errorMessage = err.data.error;
    } else {
      if (err.message) {
        errorMessage = err.message;
      } else {
        console.log("ERROR GENERICO INDEX");
      }
    }
    // toastr.error(errorMessage);
    return dispatch({
      type: types.FETCH_POST_AUTHOR,
      author: null,
    });
  }
};
