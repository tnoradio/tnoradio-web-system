import { ChatbotService } from "../../../core/chatbot/domain/ChatbotService";
import * as types from "../../types/types";
import TextInput from "../../../core/chatbot/domain/TextInput";
import Message from "../../../core/chatbot/domain/Message";
import { AxiosChatbotService } from "../../../core/chatbot/infrastructure/AxiosChatbotService";
import { TextInputRequest } from "../../../core/chatbot/application/use_cases/TextInpuRequest";

const postRepository: ChatbotService = new AxiosChatbotService();
const textInputRequester = new TextInputRequest(postRepository);

export const addOutgoingMessage = (message: string) => (dispatch) => {
  try {
    const userMessage = new Message(
      message,
      new Date(Date.now()),
      "Usuario TNO Radio",
      "outgoing",
      "normal"
    );

    dispatch({ type: types.MAKE_TEXTINPUT_REQUEST, message: userMessage });
  } catch (error) {
    console.log("addOutgoingMessage", error);
    dispatch({ type: types.MAKE_TEXTINPUT_REQUEST, message: {} });
  }
};

export const makeTextInputRequest = (message: string) => (dispatch) => {
  console.log("TEXT INPUT ACO");
  const textInput = new TextInput(message);

  textInputRequester
    .run(textInput)
    .then((data) => {
      const fulfillmentText = data[0]?.queryResult?.fulfillmentText
        ? data[0].queryResult.fulfillmentText
        : "";

      const chatMessage = new Message(
        fulfillmentText,
        new Date(Date.now()),
        "BotTNO",
        "incoming",
        "normal"
      );
      dispatch({ type: types.MAKE_TEXTINPUT_REQUEST, message: chatMessage });
    })
    .catch((err) => {
      console.log(err);
      let errorMessage = "No se pudo obtener la lista de chatbot";
      if (err.data) {
        errorMessage = err.data.error;
      } else {
        if (err.message) {
          errorMessage = err.message;
        } else {
        }
      }
      // toastr.error(errorMessage);
      dispatch({
        type: types.MAKE_TEXTINPUT_REQUEST,
        message: {},
      });
    });
};
