import { ShowRepository } from "../../../core/shows/domain/ShowRepository";
import { AxiosShowRepository } from "../../../core/shows/infrastructure/repository/AxiosShowRepository";
import { ShowImageGetter } from "../../../core/shows/application/useCases/getShowImage/showImageGetter";
import * as types from "../../types/types";
import { ShowImage } from "../../../core/shows/domain/ShowImage";

const showRepository: ShowRepository = new AxiosShowRepository();
const showImageGetter = new ShowImageGetter(showRepository);
var image: ShowImage;

export const fetchShowImageUrl = (imageName, showSlug) => {
  return (dispatch) => {
    showImageGetter
      .run(imageName, showSlug)
      .then((data) => {
        image = data;
        dispatch({ type: types.FETCH_SHOW_IMAGE_URL, imageUrl: image.url });
      })
      .catch((err) => {
        console.log("fetchShowImage ", err);
        let errorMessage = "No se pudo obtener la url";
        if (err.data) {
          errorMessage = err.data.error;
        } else {
          if (err.message) {
            errorMessage = err.message;
          } else {
          }
        }
        // toastr.error(errorMessage);
        dispatch({ type: types.FETCH_SHOW_IMAGE_URL, showMiniBanner: "" });
      });
  };
};
