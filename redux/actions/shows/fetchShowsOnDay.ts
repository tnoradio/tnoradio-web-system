import { ShowRepository } from "../../../core/shows/domain/ShowRepository";
import { AxiosShowRepository } from "../../../core/shows/infrastructure/repository/AxiosShowRepository";
import { ShowsOnWeekdayGetter } from "../../../core/shows/application/useCases/getShowsOnWeekday/showsonweekdaygetter";
import * as types from "../../types/types";
import { ShowSchedule } from "../../../core/shows/domain/ShowSchedule";
import { Show } from "../../../core/shows/domain/Show";

const showRepository: ShowRepository = new AxiosShowRepository();
const showsOnWeekday = new ShowsOnWeekdayGetter(showRepository);
var shows: Show[] = [];
var daySchedule: Show[] = [];
var weekDayParam;

export const fetchShowsOnDay = (weekday) => (dispatch) => {
  dispatch({
    type: types.CHANGE_LOAD_SCHEDULE_STATE,
    loadScheduleState: "loading",
  });
  weekDayParam = weekday;
  if (weekday === "miércoles") weekDayParam = "miercoles";
  if (weekday === "sábado" || weekday === "sabado") weekDayParam = "sabado";
  showsOnWeekday
    .run(weekDayParam)
    .then((data) => {
      shows = data;
      sortShowsListByStartTime(shows);
      dispatch({
        type: types.CHANGE_LOAD_SCHEDULE_STATE,
        loadScheduleState: "loaded",
      });
      dispatch({
        type: types.SHOWS_FILTERED_BY_DAY,
        showsOnWeekdayList: daySchedule,
      });
    })
    .catch((err) => {
      //console.log(err);
      let errorMessage = "No se pudo obtener la lista de shows";
      if (err.data) {
        errorMessage = err.data.error;
      } else {
        if (err.message) {
          errorMessage = err.message;
        } else {
          console.log("ERROR GENERICO INDEX");
        }
      }
      // toastr.error(errorMessage);
      dispatch({ type: types.SHOWS_FILTERED_BY_DAY, showsOnWeekdayList: [] });
      dispatch({
        type: types.CHANGE_LOAD_SCHEDULE_STATE,
        loadScheduleState: "notLoaded",
      });
    });
};

/**
 * This will sort shows by init time once data is stored correctly
 * in database.
 * @param shows
 */
function sortShowsListByStartTime(shows: Show[]) {
  daySchedule = [];

  shows.forEach((show) => {
    sortSchedule(show.showSchedules);
  });

  shows.forEach((show) => {
    show.showSchedules.forEach((showSchedule) => {
      if (showSchedule.weekDay.localeCompare(weekDayParam) === 0)
        daySchedule.push(show);
    });
  });
}

function sortSchedule(schedules: ShowSchedule[]) {
  schedules.sort(function (a, b) {
    var keyA = a.startTime;
    var keyB = b.startTime;
    // Compare the 2 dates
    if (keyA < keyB) return -1;
    if (keyA > keyB) return 1;
    return 0;
  });
}
