import { ShowRepository } from "../../../core/shows/domain/ShowRepository";
import { AxiosShowRepository } from "../../../core/shows/infrastructure/repository/AxiosShowRepository";
import { ShowImageGetter } from "../../../core/shows/application/useCases/getShowImage/showImageGetter";
import * as types from "../../types/types";
import { ShowImage } from "../../../core/shows/domain/ShowImage";

const showRepository: ShowRepository = new AxiosShowRepository();
const showImageGetter = new ShowImageGetter(showRepository);
var image: ShowImage;

export const fetchShowImage = (imageName, showSlug) => {
  return showImageGetter
    .run(imageName, showSlug)
    .then((data) => {
      image = data;
      return image;
    })
    .catch((err) => {
      console.log(err);
      let errorMessage = "No se pudo obtener la url";
      if (err.data) {
        errorMessage = err.data.error;
      } else {
        if (err.message) {
          errorMessage = err.message;
        } else {
        }
      }
      return null;
    });
};
