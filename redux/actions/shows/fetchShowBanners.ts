import { ShowRepository } from "../../../core/shows/domain/ShowRepository";
import { AxiosShowRepository } from "../../../core/shows/infrastructure/repository/AxiosShowRepository";
import { ShowBannersGetter } from "../../../core/shows/application/useCases/getShowBanners/showBannersGetter";
import * as types from "../../types/types";
import { ShowImage } from "../../../core/shows/domain/ShowImage";

const showRepository: ShowRepository = new AxiosShowRepository();
const showBannersGetter = new ShowBannersGetter(showRepository);
var banners: ShowImage[] = [];

export const fetchShowBanners = (imageName) => {
  return (dispatch) => {
    if (imageName === "showBanner") {
      showBannersGetter
        .run(imageName)
        .then((data) => {
          console.log(data)
          banners = data;
          dispatch({ type: types.FETCH_SHOW_BANNERS, showBanners: banners });
        })
        .catch((err) => {
          console.log("fetchShowBanners ", err);
          let errorMessage = "No se pudo obtener la lista de banners";
          if (err.data) {
            errorMessage = err.data.error;
          } else {
            if (err.message) {
              errorMessage = err.message;
            } else {
            }
          }
          // toastr.error(errorMessage);
          dispatch({ type: types.FETCH_SHOW_BANNERS, showBanners: [] });
        });
    }
    if (imageName === "showResponsiveBanner") {
      showBannersGetter
        .run(imageName)
        .then((data) => {
          console.log(data)
          banners = data;
          dispatch({
            type: types.FETCH_SHOW_RESPONSIVE_BANNERS,
            showResponsiveBanners: banners,
          });
        })
        .catch((err) => {
          console.log("showResponsiveBanner ", err);
          let errorMessage = "No se pudo obtener la lista de banners";
          if (err.data) {
            errorMessage = err.data.error;
          } else {
            if (err.message) {
              errorMessage = err.message;
            } else {
            }
          }
          // toastr.error(errorMessage);
          dispatch({
            type: types.FETCH_SHOW_RESPONSIVE_BANNERS,
            showResponsiveBanners: [],
          });
        });
    }
  };
};
