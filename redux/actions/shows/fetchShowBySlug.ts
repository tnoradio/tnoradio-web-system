import { AxiosShowRepository } from "../../../core/shows/infrastructure/repository/AxiosShowRepository";
import { DetailShowGetter } from "../../../core/shows/application/useCases/getShowDetail/detailshowgetter";
import { ShowRepository } from "../../../core/shows/domain/ShowRepository";
import * as types from "../../types/types";

const showRepository: ShowRepository = new AxiosShowRepository();
const detailShowGetter: DetailShowGetter = new DetailShowGetter(showRepository);
let showBySlug = {};

export const fetchShowBySlug = (slug) => async (dispatch) => {
  try {
    showBySlug = await detailShowGetter.run(slug);
    return dispatch({ type: types.FETCH_SHOW_BY_SLUG, showBySlug: showBySlug });
  } catch (err) {
    console.log("fetchShowBySlug ", err);
    let errorMessage = "No se pudo obtener el show";
    if (err.data) {
      errorMessage = err.data.error;
    } else {
      if (err.message) {
        errorMessage = err.message;
      } else {
        console.log("ERROR GENERICO INDEX");
      }
    }
    // toastr.error(errorMessage);
    return dispatch({
      type: types.FETCH_SHOW_BY_SLUG,
      showBySlug: "not Found",
    });
  }
};
