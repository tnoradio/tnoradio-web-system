import { ShowRepository } from '../../../core/shows/domain/ShowRepository'
import { AxiosShowRepository } from '../../../core/shows/infrastructure/repository/AxiosShowRepository'
import { EnabledShowsGetter } from '../../../core/shows/application/useCases/getEnabledShows/enabledShowsGetter'
import * as types from '../../types/types'
import { Show } from '../../../core/shows/domain/Show'

const showRepository: ShowRepository = new AxiosShowRepository();
const indexEnabledShows = new EnabledShowsGetter(showRepository);
var shows: Show[] = [];

export const fetchShows = () => dispatch => {
   indexEnabledShows.run().then(
        data => {
           shows=data;
           dispatch({type: types.FETCH_SHOWS, showsList: shows});        
       
    }).catch(err => {
       console.log(err);
       let errorMessage = "No se pudo obtener la lista de shows";
       if (err.data) {
          errorMessage = err.data.error;
       }
       else {
          if(err.message) {
             errorMessage = err.message
          }else {
          }
       }
      // toastr.error(errorMessage);
       dispatch ({
          type: types.FETCH_SHOWS,
          shows:[]
      });   
  }); 
}
  

 
  
