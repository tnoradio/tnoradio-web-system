import { ShowRepository } from "../../../core/shows/domain/ShowRepository";
import { AxiosShowRepository } from "../../../core/shows/infrastructure/repository/AxiosShowRepository";
import { OnlineShowByTimeGetter } from "../../../core/shows/application/useCases/getOnlineShowByTime/onlineShowByTimeGetter";
import * as types from "../../types/types";
import { Show } from "../../../core/shows/domain/Show";

const showRepository: ShowRepository = new AxiosShowRepository();
const onlineShowByTimeGetter = new OnlineShowByTimeGetter(showRepository);
var show: Show;

export const showOnline = () => (dispatch) => {
  var time = new Date(Date.now()).toString();

  onlineShowByTimeGetter
    .run(time)
    .then((data) => {
      console.log("fetchOnlineShowByTime ", data);
      show = data;
      dispatch({ type: types.ONLINE_SHOW, showOnline: show });
    })
    .catch((err) => {
      console.log("fetchOnlineShowByTime ", err);
      let errorMessage = "No se pudo obtener la lista de shows";
      if (err.data) {
        errorMessage = err.data.error;
      } else {
        if (err.message) {
          errorMessage = err.message;
        } else {
          console.log("ERROR GENERICO INDEX");
        }
      }
      // toastr.error(errorMessage);
      dispatch({
        type: types.ONLINE_SHOW,
        showOnline: null,
      });
    });
};
