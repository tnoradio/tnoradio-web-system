import { OnDemandRepository } from '../../../core/shows/domain/OnDemandRepository'
import { AxiosOnDemandRepository } from '../../../core/shows/infrastructure/repository/AxiosOnDemandRepository'
import { AllShowFileNamesGetter } from '../../../core/shows/application/useCases/getShowFileNames/allshowfilenamesgetter'
import * as types from '../../types/types'

const showRepository: OnDemandRepository = new AxiosOnDemandRepository();
const showFileNamesGetter = new AllShowFileNamesGetter(showRepository);

export const fetchShowFileNames = (showSlug: String) => (dispatch) => {
    console.log("showSlug in names", showSlug)
    showFileNamesGetter
    .run(showSlug)
    .then(data => {
        console.log(data);
        dispatch({
            type: types.FETCH_SHOW_FILE_NAMES, 
            showFileNames: data
        });        
    }).catch(err => {
       console.log(err);
       let errorMessage = "No se pudo obtener la lista de shows";
       if (err.data) {
          errorMessage = err.data.error;
       }
       else {
          if(err.message) {
             errorMessage = err.message
          }else {
          }
       }
      // toastr.error(errorMessage);
       dispatch ({
          type: types.FETCH_SHOW_FILE_NAMES,
          showFileNames:[]
      });   
  }); 
}
  

 
  
