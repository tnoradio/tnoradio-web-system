import { AxiosUserRepository } from '../../../core/users/infrastructure/repository/AxiosUserRepository'
import { DetailUserGetter } from '../../../core/users/application/use_cases/getUserDetail/detailusergetter'
import { UserRepository } from '../../../core/users/domain/UserRepository'
import { User } from '../../../core/users/domain/User'
import * as types from '../../types/types'

const userRepository: UserRepository = new AxiosUserRepository();
const detailUserGetter: DetailUserGetter = new DetailUserGetter(userRepository);
let userBySlug: User;

export const fetchUserBySlug = slug => async dispatch => {

try{
   userBySlug = await detailUserGetter.run(slug);
   return dispatch({type: types.FETCH_USER_BY_SLUG, userBySlug: userBySlug}); 
}
 catch(err) {
      console.log(err);
      let errorMessage = "No se pudo obtener la lista de users";
      if (err.data) {
         errorMessage = err.data.error;
      }
      else {
         if(err.message) {
            errorMessage = err.message
         }else {
            console.log("ERROR GENERICO INDEX");
         }
      }
      // toastr.error(errorMessage);
      return dispatch ({
         type: types.FETCH_USER_BY_SLUG,
         userBySlug: "not Found"
      });   
   };
}