import { UserRepository } from "../../../core/users/domain/UserRepository";
import { AxiosUserRepository } from "../../../core/users/infrastructure/repository/AxiosUserRepository";
import { HostsGetter } from "../../../core/users/application/use_cases/hostsGetter/getHosts";
import * as types from "../../types/types";

const userRepository: UserRepository = new AxiosUserRepository();
const hostsGetter = new HostsGetter(userRepository);

export const fetchHosts = () => (dispatch) => {
  hostsGetter
    .run()
    .then((data) => dispatch({ type: types.FETCH_HOSTS, hosts: data }))
    .catch((err) => {
      console.log("fetchHosts", err);
      let errorMessage = "No se pudo obtener la lista de users";
      if (err.data) {
        errorMessage = err.data.error;
      } else {
        if (err.message) {
          errorMessage = err.message;
        } else {
        }
      }
      // toastr.error(errorMessage);
      dispatch({
        type: types.FETCH_HOSTS,
        hosts: [],
      });
    });
};
