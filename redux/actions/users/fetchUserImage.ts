import { UserRepository } from "../../../core/users/domain/UserRepository";
import { AxiosUserRepository } from "../../../core/users/infrastructure/repository/AxiosUserRepository";
import { UserImageFromDBGetter } from "../../../core/users/application/use_cases/userimagefromdbgetter/getUserImageFromDB";
import * as types from "../../types/types";

const userRepository: UserRepository = new AxiosUserRepository();
const userImageFromDBGetter = new UserImageFromDBGetter(userRepository);

const fetchUserImage = (name, slug) => async (dispatch) => {
  try {
    const userImage = await userImageFromDBGetter.run(name, slug);
    return dispatch({ type: types.FETCH_USER_IMAGE, userImage: userImage });
  } catch (err) {
    console.log("fetchUserImage ", err);
    let errorMessage = "No se pudo obtener la lista de users";
    if (err.data) {
      errorMessage = err.data.error;
    } else {
      if (err.message) {
        errorMessage = err.message;
      } else {
        console.log("ERROR GENERICO INDEX");
      }
    }
    // toastr.error(errorMessage);
    return dispatch({
      type: types.FETCH_USER_IMAGE,
      userBySlug: "not Found",
    });
  }
};

export default fetchUserImage;
