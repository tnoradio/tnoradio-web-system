import { UserRepository } from "../../../core/users/domain/UserRepository";
import { AxiosUserRepository } from "../../../core/users/infrastructure/repository/AxiosUserRepository";
import { TeamGetter } from "../../../core/users/application/use_cases/teamGetter/getTeam";
import * as types from "../../types/types";

const userRepository: UserRepository = new AxiosUserRepository();
const teamGetter = new TeamGetter(userRepository);

export const fetchTeam = () => (dispatch) => {
  teamGetter
    .run()
    .then((data) => dispatch({ type: types.FETCH_TEAM, team: data }))
    .catch((err) => {
      console.log("fetchTeam ", err);
      let errorMessage = "No se pudo obtener la lista de users";
      if (err.data) {
        errorMessage = err.data.error;
      } else {
        if (err.message) {
          errorMessage = err.message;
        } else {
        }
      }
      // toastr.error(errorMessage);
      dispatch({
        type: types.FETCH_TEAM,
        team: [],
      });
    });
};
