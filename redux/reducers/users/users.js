import * as types from "../../types/types";

let initialState = {
  usersList: [],
  hosts: [],
  userBySlug: null,
  userById: null,
  userImage: null,
};
export const UsersReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.FETCH_USERS:
      return { ...state, usersList: action.usersList };

    case types.FETCH_USER_BY_SLUG:
      return { ...state, userBySlug: action.userBySlug };

    case types.FETCH_USER_BY_ID:
      return { ...state, userById: action.userById };

    case types.FETCH_HOSTS:
      return { ...state, hosts: action.hosts };

    case types.FETCH_TEAM:
      return { ...state, team: action.team };

    case types.FETCH_USER_IMAGE:
      return { ...state, userImage: action.userImage };

    default:
      return state;
  }
};
