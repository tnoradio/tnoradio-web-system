import * as types from "../../types/types";

let initialState = {
  showsList: [],
  showsOnWeekdayList: [],
  showBanners: [],
  miniBanners: [],
  showResponsiveBanners: [],
  showOnline: null,
  showBySlug: null,
  loadScheduleState: "",
  imageUrl: "",
  showAdvertisers: [],
  showFileNames: [],
};

export const ShowsReducer = (state = initialState, action) => {
  var miniBanners = state.miniBanners;
  switch (action.type) {
    case types.FETCH_SHOWS:
      return { ...state, showsList: action.showsList };

    case types.FETCH_SHOW_FILE_NAMES:
      return { ...state, showFileNames: action.showFileNames };

    case types.FETCH_SHOW_BANNERS:
      return { ...state, showBanners: action.showBanners };

    case types.FETCH_SHOW_MINI_BANNER:
      if (
        action.showMiniBanner !== null &&
        miniBanners.some(
          (miniBanner) => miniBanner.showSlug === action.showMiniBanner.showSlug
        ) === false
      )
        miniBanners.push(action.showMiniBanner);

      return { ...state, miniBanners: miniBanners };

    case types.FETCH_SHOW_RESPONSIVE_BANNERS:
      return { ...state, showResponsiveBanners: action.showResponsiveBanners };

    case types.ONLINE_SHOW:
      return { ...state, showOnline: action.showOnline };

    case types.FETCH_SHOW_BY_SLUG:
      return { ...state, showBySlug: action.showBySlug };

    case types.FETCH_SHOW_IMAGE_URL:
      return { ...state, imageUrl: action.imageUrl };

    case types.SHOWS_FILTERED_BY_DAY:
      return {
        ...state,
        showsOnWeekdayList: action.showsOnWeekdayList,
        miniBanners: [],
      };

    case types.CHANGE_LOAD_SCHEDULE_STATE:
      return { ...state, loadScheduleState: action.loadScheduleState };

    default:
      return state;
  }
};
