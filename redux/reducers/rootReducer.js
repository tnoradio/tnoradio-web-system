// import external modules
import { combineReducers } from "redux";
import { ShowsReducer } from "./shows/shows";
import { UsersReducer } from "./users/users";
import { PostsReducer } from "./posts/posts";
import { ChatbotReducer } from "./chat/chats";
import { sessionReducer } from "redux-react-session";
import { UtilsReducer } from "./utils/utils";

const rootReducer = combineReducers({
  postApp: PostsReducer,
  chatbotApp: ChatbotReducer,
  showApp: ShowsReducer,
  userApp: UsersReducer,
  utilsApp: UtilsReducer,
  //appSession: sessionReducer
});

export default rootReducer;
