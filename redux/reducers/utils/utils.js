import * as types from "../../types/types";

let initialState = {
  today: "lunes",
  scheduleSelectedDay: "lunes",
};
export const UtilsReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.GET_TODAY:
      console.log("UtilsReducer ", action.today);
      return { ...state, today: action.today };
    case types.SET_NAVBAR_DAY:
      return { ...state, scheduleSelectedDay: action.scheduleSelectedDay };
    default:
      return state;
  }
};
