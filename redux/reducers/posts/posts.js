import * as types from "../../types/types";

let initialState = {
  postsPage: [],
  posts: [],
  tags: [],
  postBySlug: null,
  postImage: null,
  page: 0,
  pageSize: 0,
  postAuthors: new Map(),
};
export const PostsReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.FETCH_POSTS:
      return { ...state, posts: action.posts };

    case types.FETCH_POSTS_PAGE:
      return {
        ...state,
        postsPage: action.postsPage,
        pageSize: action.pageSize,
        page: action.page,
      };

    case types.FETCH_POST_BY_SLUG:
      return { ...state, postBySlug: action.postBySlug };

    case types.FETCH_POST_BY_ID:
      return { ...state, tags: action.tags };

    case types.FETCH_POST_IMAGE:
      return { ...state, postImage: action.postImage };

    case types.FETCH_POST_AUTHOR:
      var hasAuthor = state.postAuthors.has(action.author?._id);
      var authorMap =
        hasAuthor === false
          ? state.postAuthors.set(action.author._id, {
              name: `${action.author.name} ${action.author.lastName}`,
              slug: action.author.slug,
            })
          : state.postAuthors;
      return {
        ...state,
        postAuthors: authorMap,
      };

    default:
      return state;
  }
};
