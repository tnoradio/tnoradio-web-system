import * as types from "../../types/types";

let initialState = [];

export const ChatbotReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.MAKE_TEXTINPUT_REQUEST:
      return [...state, action.message];
    case types.ADD_USER_TEXT_INPUT:
      return [...state, action.message];
    default:
      return state;
  }
};
