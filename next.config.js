module.exports = {
  webpack: (config, options) => {
    config.module.rules.push({
      test: /\.(png|jpe?g|gif)$/i,
      loader: "file-loader",
      options: {
        name: "[path][name].[ext]",
      },
    });

    return config;
  },
  images: {
    loader: "imgix",
    path: "https://shows.tnoradio.com",
  },

  async redirects() {
    return [
      {
        source: "/programacion",
        destination: "/programacion/lunes",
        permanent: true,
      },
    ];
  },
  env: {
    NEXT_API_USERS_URL: "https://api.tnoradio.com/api/users/",
    NEXT_API_SHOWS_URL: "https://shows.tnoradio.com/api/shows/",
    NEXT_API_POSTS_URL: "https://posts.tnoradio.com/api/posts/",
    NEXT_API_CHATBOT_URL: "https://api.tnoradio.com/api/chatbot/",
    NEXT_API_CDN_URL: "https://api.tnoradio.com/",
    // Top Header Post
    NEXT_GOOGLE_DATA_AD_CLIENT: "ca-pub-2964421302099607",
    NEXT_GOOGLE_ADD_DATA_SLOT: "7691510301",
  },
};
