import isValidEmail from './ValidateEmail'
import { isValidPass } from './ValidatePassword'
import { passwordsMatch } from './ValidatePassword'
import isValidPhone from './ValidatePhoneNumber'
import isValidString from './ValidateString'
import isNotEmpty from './NotEmptyValidation'

export default { isValidEmail, isValidPass, passwordsMatch, isValidPhone, isValidString, isNotEmpty }