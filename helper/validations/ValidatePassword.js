export function isValidPass(pass) {
    const regExpPass = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;
       return RegExp(regExpPass).test(pass);
}

export function passwordsMatch(password, passwordConfirmation) {
        return password == passwordConfirmation;
}