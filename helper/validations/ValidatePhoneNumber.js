export default function isValidPhone(phone) {
    function validateAllNumbers() {
        const regExpOnlyNumbers = /^[0-9]+$/;
        return phone.value.match(regExpOnlyNumbers);
    }
    function validateNumberCount() {
        const  regExpTenNumbers = /^\d{10}$/; 
        return phone.value.match(regExpTenNumbers);
    }
    return (validateAllNumbers && validateNumberCount);
}