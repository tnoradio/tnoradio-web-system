export default function isNotEmpty(text) {
  return text.value != null && text.value.length != 0;
}
