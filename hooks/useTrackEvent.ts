import { useCallback } from "react";

declare const window: any;
export function useTrackEvent() {
  const trackEvent = useCallback(
    (name: string, category: string, action: string, label: string) => {
      window.dataLayer.push({
        event: name,
        eventCategory: category,
        eventAction: action,
        nonInteraction: false,
        eventLabel: label,
      });
      console.log({
        event: name,
        eventCategory: category,
        eventAction: action,
        nonInteraction: false,
        eventLabel: label,
      });
    },
    []
  );

  return { trackEvent };
}
