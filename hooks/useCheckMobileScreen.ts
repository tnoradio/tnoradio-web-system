import React, {useEffect, useState} from "react";

declare const window: any;
const useCheckMobileScreen = () => {

    const [width, setWidth] = typeof window !== "undefined" ? useState(window.innerWidth) : useState(0);
    
    const handleWindowSizeChange = () => {
        setWidth(window.innerWidth);
    }

    useEffect(() => {
        console.log("Screen size chaned");
        window.addEventListener('resize', handleWindowSizeChange);
        return () => {
            window.removeEventListener('resize', handleWindowSizeChange);
        }
    }, []);

    return (width <= 560);
}

export default useCheckMobileScreen;